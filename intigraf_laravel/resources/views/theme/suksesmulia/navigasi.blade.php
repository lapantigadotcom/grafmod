 <nav class="navigasi navbar navbar-default navbar-fixed-top navbar-custom">
  <div class="container-fluid container">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">
        <img src="{{asset('images/logo.png')}}">
      </a>
    </div>
    <ul class="nav navbar-nav pull-right">
      <li><a href="{{url('/')}}#">HOME</a></li>
      <li><a href="{{url('/')}}#tower">TOWER</a></li>
      <li><a href="{{url('/')}}#layoutunit">LAYOUT UNIT</a></li>
      <li><a href="{{url('/')}}#galeri">GALLERY</a></li>
      <li><a href="{{url('/')}}#event">EVENTS</a></li>
      <li><a href="{{url('/')}}#experience">EXPERIENCE</a></li>
      <li><a href="{{url('/')}}#contact">CONTACT</a></li>
      <li class="language_id"> <span><a title="BAHASA" href="?ln=id">BAHASA </a> 
      </span><span> / </span><span><a title="ENGLISH" href="?ln=eng" >ENG</a>  </span></li>
    </ul>
  </div>
</nav>
<section class="navigasi"  style="margin-bottom:0">
 <div class="">
  <div class="col-md-6 col-md-offset-3">
     
   </div>
</div>
</section>