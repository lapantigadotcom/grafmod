  @foreach($data['latest_event'] as $row)
  <article class="post media-post clearfix pb-0 mb-10">
    <a class="post-thumb" href="#">@if (isset($d->mediaPost->file))
                        <img style="max-width:  75px" src="{{asset('upload/media/'.$d->mediaPost->file)}}" alt="" class="img-responsive img-fullwidth"> 
                        @else
                         <img src="{{asset('assets/tema/212mart_sby/images/no-image.jpg')}}" alt="" class="img-responsive img-fullwidth" style="max-width:  75px"> 
                       @endif
    <div class="post-right">
       <h4 class="post-title mt-0">
      <a title="{{ $row->title }}" href="{{ url('/news/'. $row->id) }}">
      @if ($data['english'])
      {!! substr(strip_tags($row->title_eng),0, 20).'...' !!}
      @else
      {!! substr(strip_tags($row->title),0, 20).'...' !!}
      @endif
    </a></h4>
       <p>@if ($data['english'])
      {!! substr(strip_tags($row->eng_description),0, 100).'...' !!}</p>
      @else
      {!! substr(strip_tags($row->description),0, 100).'...' !!}
      @endif </p>
    </div>
  </article>
  @endforeach