@section('meta_title', 'Galeri Photo')
@section('og-image')
<meta property="og:image" content="{{ asset('assets/tema/212mart_sby/images/logo-kks212.png') }}" />
@endsection
@section('meta_desc')
<meta name="description" content="Membangun Ekonomi Qurani yang besar, kuat, professional dan terpercaya sebagai salah satu penopang pilar ibadah, syariah dan dakwah menuju kebahagiaan dunia dan keselamatan akhirat"/> 
@endsection
@section('meta_key')
<meta name="keywords" content="kks212sby,koperasi,koperasi syariah, komunitas syariah"/>
@endsection
@extends('theme.kks212sby.main')
@section('content')

 <section>
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <!-- Works Filter -->
            <div class="portfolio-filter font-alt align-center">
              <a href="#" class="active" data-filter="*">Galeri Photo Korlantas</a>
                 <!-- End Works Filter -->
            
            <!-- Portfolio Gallery Grid -->
              </div>
             <div id="grid" class="gallery-isotope grid-3 gutter clearfix">
              <?php 
            $i = 1;
            ?>
             @foreach($data['group-portfolio'] as $row)
             <?php
             
             if(($i-1)%4 == 0)
               ;
             ?>
               
               
                <p class="text-center">Title:{{ $row->title}}</p><hr>
                <div class="thumb"> 
                  <img src="{{asset('assets/tema/212mart_sby/images/no-image.jpg')}}" href="{{ route('home.portfolio', $row->id) }}">
                   <div class="overlay-shade" width="250px"></div> 
                  
                  <div class="icons-holder">
                    <div class="icons-holder-inner">
                      <div class="text-white">
                     <a style="color:#ffffff" href="{{ route('home.portfolio', $row->id) }}"><i class="fa fa-eye"></i>&nbsp;View Gallery</a> 
                       </div>
                    </div>
                  </div>
                   
                </div> @endforeach
              </div>

              
              <!-- Portfolio Item End --> 
            </div><!-- End Portfolio Gallery Grid -->

                      <div class="row" id="paginator">

        {!! $data['group-portfolio']->render() !!}
      </div>
   

    </section>
@endsection

@section('custom-js')

@endsection



