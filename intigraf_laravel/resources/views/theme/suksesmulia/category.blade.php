@section('meta_title', 'Event & News')
@section('og-image')
<meta property="og:image" content="{{ asset('assets/tema/212mart_sby/images/logo-kks212.png') }}" />
@endsection
@section('meta_desc')
<meta name="description" content="Membangun Ekonomi Qurani yang besar, kuat, professional dan terpercaya sebagai salah satu penopang pilar ibadah, syariah dan dakwah menuju kebahagiaan dunia dan keselamatan akhirat"/> 
@endsection
@section('meta_key')
<meta name="keywords" content="kks212sby,koperasi,koperasi syariah, komunitas syariah"/>
@endsection
@extends('theme.kks212sby.main')
@section('content')


<!-- Section: Blog -->
<section>
  <div class="container mt-30 mb-30 pt-30 pb-30">

    <div class="row">
      <div class="col-md-3">
        <div class="sidebar sidebar-left mt-sm-30">

          <div class="widget">
            <h5 class="widget-title line-bottom">Twitter Feed</h5>
            <div class="twitter-feed list-border clearfix" data-username="Envato" data-count="3"></div>
          </div>

          <div class="widget">
            <h5 class="widget-title line-bottom">Tags</h5>
            <div class="tags">


            </div>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <article class="post clearfix mb-30 pb-30">
         @foreach($data['content']->sortByDesc('created_at') as $v)
         @if($v->ms_media_id != '0')
         <div class="entry-header">
          <div class="post-thumb thumb"> 
            <img src="{{ asset('upload/media/'.$v->mediaPost->file) }}" alt="" class="img-responsive img-fullwidth"> 
          </div>
        </div>
        <div class="entry-content border-1px p-20 pr-10">
          <div class="entry-meta media mt-0 no-bg no-border">
            <div class="entry-date media-left text-center flip bg-theme-colored pt-5 pr-15 pb-5 pl-15">
              <ul>
                <li class="font-16 text-white font-weight-600">{{ date("d" ,strtotime($v->created_at)) }}</li>
                <li class="font-12 text-white text-uppercase">{{ date("M" ,strtotime($v->created_at)) }}</li>
              </ul>
            </div>
            <div class="media-body pl-15">
              <div class="event-content pull-left flip">
                <h4 class="entry-title text-biru text-uppercase m-0 mt-5"><a href="#">@if ($data['english'])
                  {!! substr(strip_tags($v->title_eng),0, 60).'...' !!}
                  @else
                  {!! substr(strip_tags($v->title),0, 60).'...' !!}
                @endif</a></h4>
                <span class="mb-10 text-gray-darkgray mr-10 font-13"><i class="fa fa-user mr-5 text-theme-colored"></i>{{ $v->user->name }}</span>      
              </div>
            </div>
          </div>
          <p class="mt-10"> @if ($data['english'])
            <?php $desc = explode("<!-- pagebreak -->", $v->eng_description); ?>
            @else
            <?php $desc = explode("<!-- pagebreak -->", $v->description); ?>
            @endif
            <?php
            $desc = explode("</p>", $desc[0]);
            ?>
            @if(strlen($desc[0]) > 400)
            {!! substr($desc[0], 0, 400) !!}...
            @else
            {!! $desc[0] !!}&nbsp;
          @endif</p>
          @if ($data['english'])
          <a href="{!! route('home.post',[ $v->id,'post' ]) !!}" class="btn-read-more">Read More</a>
          @else
          <a href="{{URL::to('/212/artikel/'.$v->id.'/'.str_replace(' ',$v->title))}}" class="btn-read-more">Selengkapnya</a>
          @endif 
          <div class="clearfix"></div>
        </div>
      </article> 
      @else
      <article class="post clearfix mb-30 pb-30">
        <div class="entry-header">
          <div class="post-thumb thumb"> 
            <img src="{{asset('assets/tema/212mart_sby/images/no-image.jpg')}}" alt="" class="img-responsive img-fullwidth"> 
          </div>
        </div>
        <div class="entry-content border-1px p-20 pr-10">
          <div class="entry-meta media mt-0 no-bg no-border">
            <div class="entry-date media-left text-center flip bg-theme-colored pt-5 pr-15 pb-5 pl-15">
              <ul>
                <li class="font-16 text-white font-weight-600">{{ date("d" ,strtotime($v->created_at)) }}</li>
                <li class="font-12 text-white text-uppercase">{{ date("M" ,strtotime($v->created_at)) }}</li>
              </ul>
            </div>
            <div class="media-body pl-15">
              <div class="event-content pull-left flip">
                <h4 class="entry-title text-white text-uppercase m-0 mt-5"><a href="#">@if ($data['english'])
                  {!! substr(strip_tags($v->title_eng),0, 60).'...' !!}
                  @else
                  {!! substr(strip_tags($v->title),0, 60).'...' !!}
                @endif</a></h4>
                <span class="mb-10 text-gray-darkgray mr-10 font-13"><i class="fa fa-user mr-5 text-theme-colored"></i>{{ $v->user->name }}</span>      
              </div>
            </div>
          </div>
          <p class="mt-10"> @if ($data['english'])
            <?php $desc = explode("<!-- pagebreak -->", $v->eng_description); ?>
            @else
            <?php $desc = explode("<!-- pagebreak -->", $v->description); ?>
            @endif
            <?php
            $desc = explode("</p>", $desc[0]);
            ?>
            @if(strlen($desc[0]) > 500)
            {!! substr($desc[0], 0, 500) !!}...
            @else
            {!! $desc[0] !!}&nbsp;
          @endif</p>
          @if ($data['english'])
          <a href="{!! route('home.post',[ $v->id,'post' ]) !!}" class="btn-read-more">Read More</a>
          @else
          <a href="{!! route('home.post',[ $v->id,'post' ]) !!}" class="btn-read-more">Selengkapnya</a>
          @endif 
          <div class="clearfix"></div>
        </div>
        @endif
        @endforeach
      </article>  
      <div class="row" id="paginator">
        <center>
          {!! $data['content']->render() !!}
        </center>
      </div>
    </div>
    <div class="col-md-3">
      <div class="sidebar sidebar-right mt-sm-30">
        <div class="widget">
          <h5 class="widget-title line-bottom">Cari Artikel</h5>
          <div class="search-form"> 
              
             {!! Form::open(array('url' => '/cari','method' => 'get'), ['class' => 'newsletter-form mt-10']) !!}
            <div class="input-group">
              <?php
              $value = null;
              if (isset($data['keyword']))
                $value = $data['keyword'];
              ?>
              {!! Form::text('keyword',$value, ['class' => 'form-control', 'placeholder' => 'Cari Berita/Artikel...', 'class' => 'form-control ','data-height' => '20px']) !!}
              <span class="input-group-btn">
                {!! Form::submit('Cari',[ 'class' => 'btn search-button', 'data-height' => '40px']) !!}
              </span>
            </div>
          {!! Form::close()!!}


          </div>
        </div>
        <div class="widget">
          <h5 class="widget-title line-bottom">Kategori</h5>
          <div class="categories">
            <ul class="list list-border angle-double-right">
              <li><a href="#">Event<span>(19)</span></a></li>
              <li><a href="#">Informasi<span>(21)</span></a></li>
              <li><a href="#">Berita<span>(15)</span></a></li>
            </ul>
          </div>
        </div>
        <div class="widget">
          <h5 class="widget-title line-bottom">Informasi Terbaru</h5>
          <div class="latest-posts">
            @include('theme.kks212sby.sidebar_blog')
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</section>

@endsection