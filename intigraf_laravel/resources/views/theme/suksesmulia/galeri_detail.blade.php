@section('meta_title', 'Agenda Kegiatan')
@section('og-image')
<meta property="og:image" content="{{ asset('assets/tema/212mart_sby/images/logo-kks212.png') }}" />
@endsection
@section('meta_desc')
<meta name="description" content="Membangun Ekonomi Qurani yang besar, kuat, professional dan terpercaya sebagai salah satu penopang pilar ibadah, syariah dan dakwah menuju kebahagiaan dunia dan keselamatan akhirat"/> 
@endsection
@section('meta_key')
<meta name="keywords" content="kks212sby,koperasi,koperasi syariah, komunitas syariah"/>
@endsection
@extends('theme.kks212sby.main')
@section('content')

 
  <div class="page-container">
    <!-- BEGIN BREADCRUMBS -->
    <div class="row breadcrumbs margin-bottom-40">
      <div class="container">
          <div class="col-md-4 col-sm-4">
              <h1>Korlantas</h1>
          </div>
          <div class="col-md-8 col-sm-8">
              <ul class="pull-right breadcrumb">
                  <li><a href="{!! URL::to('/') !!}">Home</a></li>
                   <li class="active">Media</li>
                   <li class="active">{!! $data['content']->title !!}</li>
              </ul>
          </div>
      </div>
  </div>
  <!-- END BREADCRUMBS -->
  <div class="row">
    <div class="container min-hight portfolio-page margin-bottom-40">
      <div class="filter-v1">
        <ul class="mix-filter">
          <li class="filter active" data-filter="all">Galeri Korlantas</li>
          <hr>
        </ul>
        <div class="row mix-grid thumbnails"> 
          @foreach($data['portfolio'] as $row)
            <div style="display: inline-block;  opacity: 1;" class="col-md-4 col-sm-4 mix  mix_all">
              <div class="mix-inner"> 
                @if(file_exists('./upload/'.$row->file))
                <img src="{{ asset('upload/media/'.$row->media->file) }}" class="img-responsive" title="{{ $row->caption}}"> @endif
                <div class="mix-details">
                  <h4>{{ $row->title}}</h4>
                  <a class="mix-link" href="@if($row->type == '1' ) {{ asset('upload/media/'.$row->media->file) }} @else{!! url( trim($row->link) ) !!}@endif"><i class="icon-link"></i></a>
                  <a class="mix-preview fancybox-button" href="{{ asset('upload/media/'.$row->media->file) }}" title="{{ $row->title}}" data-rel="fancybox-button"><i class="icon-search"></i></a>
                </div>
              </div>
            </div>
          @endforeach
        </div>
      </div>
    </div>
    <div class="row" id="paginator">
      <center>
        {!! $data['portfolio']->render() !!}
      </center>
    </div>
  </div>

  </div></div>
 

        @endsection

        @section('custom-head')
         
        @endsection

        @section('footer_tambahan')
         <script type="text/javascript" src="{{ asset('assets/tema/korlantas/plugins/jquery.mixitup.min.js')}}"></script>
       <script type="text/javascript" src="{{ asset('assets/tema/korlantas/scripts/portfolio.js')}}"></script>

    <script type="text/javascript">
        jQuery(document).ready(function() {
            App.init();
            Portfolio.init();                      
        });
    </script>
        
        @endsection