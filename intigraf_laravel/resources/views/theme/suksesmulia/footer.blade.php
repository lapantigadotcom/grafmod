 <!-- Footer -->
 <footer id="footer" class="footer divider" style="background: #0A4595">
  <div class="container">
    <div class="row border-bottom">
      <div class="col-sm-6 col-md-3">
        <div class="widget dark">
          <img class="mt-5 mb-20" alt="" src="images/logo-white-footer.png">
          <h4 class="widget-title">Sekretariat</h4>
          <ul class="list-inline mt-5 text-white">
            <li class="m-0 pl-10 pr-10"> <i class="fa fa-home text-hijau mr-5"></i>Purimas Regency Blok B-3 No 57 
              Gunung Anyar 
              Surabaya  Jawa Timur, 
              Indonesia
            </li>
            <li class="m-0 pl-10 pr-10"> <i class="fa fa-envelope-o text-hijau mr-5"></i> daftar212martsby@gmail.com</li>
            <li class="m-0 pl-10 pr-10"> <i class="fa fa-globe text-hijau mr-5"></i>www.kks212sby.com</li>
          </ul>
        </div>
      </div>
      <div class="col-sm-6 col-md-3">
        <div class="widget dark">
          <h4 class="widget-title">Tautan Lain</h4>
          <ul class="list angle-double-right list-border text-white">
            <li><a href="#">Pengurus</a></li>
            <li><a href="#">Kemitraan</a></li>
            <li><a href="#">Ruang Anggota</a></li>
            <li><a href="#">Galeri</a></li>
            <li><a href="#">Lokasi Gerai</a></li>              
          </ul>
        </div>
      </div>
      <div class="col-sm-6 col-md-3">
        <div class="widget dark">
          <h4 class="widget-title">Twitter 212</h4>
          <div class="twitter-feed list-border clearfix" data-username="lapantigadotcom" data-count="2"></div>
        </div>
      </div>
      <div class="col-sm-6 col-md-3">
        <div class="widget dark">
          <img class="mt-5 mb-20" alt="" src="{{asset ('assets/tema/212mart_sby/images/212mart_logo.png')}}" style="max-height: 55px">
          <div class="opening-hours">
            <ul class="list-border text-white">
             Membangun Ekonomi Qurani yang besar, kuat, professional dan terpercaya sebagai salah satu penopang pilar ibadah, syariah dan dakwah menuju kebahagiaan dunia dan keselamatan akhirat.
           </ul>
         </div>
       </div>
     </div>
   </div>
   <div class="row mt-30">
    <div class="col-md-2">
      <div class="widget dark">
        <h5 class="widget-title mb-10">Kontak SMS/WA:</h5>
        <div class="text-white">
          081331551900 <br> 08123012582 <br>

        </div>
      </div>
    </div>
    <div class="col-md-3">
      <div class="widget dark">
        <h5 class="widget-title mb-10">Terhubung Dengan Kami</h5>
        <ul class="styled-icons icon-bordered icon-sm">
          <li class="text-white"><a href="#"><i class="fa fa-facebook"></i></a></li>
          <li class="text-white"><a href="#"><i class="fa fa-twitter"></i></a></li>
        </ul>
      </div>
    </div>
    <div class="col-md-5 col-md-offset-2">
      <div class="widget dark">
        <h5 class="widget-title mb-10">Berlangganan Informasi</h5>
        <form method="post" action="{{ url('subscribe') }}" class="newsletter-form">
          <div class="input-group">
            {!! csrf_field() !!}
            <input type="text" class="form-control input-lg font-16" data-height="45px" name="email" placeholder="Email Anda..." required="">
            <span class="input-group-btn">
              <input type="submit" data-height="45px" class="btn bg-theme-color-2 text-white btn-xs m-0 font-14" value="Subscribe !">
            </span>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<div class="footer-bottom bg-hijau">
  <div class="container pt-20 pb-20">
    <div class="row">
      <div class="col-md-6">
        <p class="font-11 text-white m-0">Copyright &copy;2017 KKS212 Surabaya. All Rights Reserved</p>
      </div>
      <div class="col-md-6 text-right text-white">
        <div class="widget no-border m-0">
          <ul class="list-inline sm-text-center mt-5 font-12">
            <li>
              <a href="#"><i class="fa fa-exclamation-circle"></i> FAQ</a>
            </li>
            <li>|</li>
            <li>
              <a href="#"><i class="fa fa-pencil"></i> Panduan</a>
            </li>
            <li>|</li>
            <li>
              <a href="{{url ('/gerai')}}"><i class="fa fa-map-marker"></i> Lokasi Gerai 212Mart</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
</footer>
<a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
</div>
  <!-- end wrapper -->