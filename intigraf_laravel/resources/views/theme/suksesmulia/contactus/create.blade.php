@extends('theme.kks212sby.main')
@section('meta_title', 'Hubungi Kami')
@section('content')
   <section class="divider">
      <div class="container pt-20">
         
        <div class="row pt-10">
          <div class="col-md-5">
          <h4 class="mt-0 mb-30 line-bottom">Sekretariat <a style="float: right; color: red" href="{{url ('/gerai')}}" ><i class="fa fa-map-marker"></i> Lokasi Gerai 212 Mart</a> </h4>
         <div 
            data-address="Purimas Regency Blok B-3 No 57 Gunung Anyar Surabaya Jawa Timur, Indonesia "
            data-popupstring-id="#popupstring1"
            class="map-canvas autoload-map"
            data-mapstyle="212map"
            data-height="420"
            data-latlng="-7.3353332,112.7816617"
            data-title="212 Mart"
            data-zoom="15"
            data-marker="images/map_marker212.png">   
          </div>
          <div class="map-popupstring hidden" id="popupstring1">
            <div class="text-center">
              <h3>Sekretariat</h3>
              <p>Purimas Regency Blok B-3 No 57 Gunung Anyar Surabaya Jawa Timur, Indonesia </p>
            </div>
          </div>
           
          <script src="http://maps.google.com/maps/api/js?key=AIzaSyDlS_YgMMNGsQkQbVijCfPcH4EtKwcdxoo"></script>
                <script src="{{asset('assets/tema/212mart_sby/js/google-map-init.js')}}"></script>

          </div>
          
          @include('212mart.partials.formcontact')
        </div>
      </div>
    </section>

@endsection

@section('custom-head')
 
@endsection

@section('custom-footer')
 
@endsection