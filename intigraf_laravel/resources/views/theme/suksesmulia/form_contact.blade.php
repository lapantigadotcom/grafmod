 

       <form>
            <div class="form-group">
              <label>Name:</label>
              <input type= "text" name="name" class="form-control col-sm-12 form-custom-style" placeholder="Enter name">
            </div>
            <div class="form-group">
              <label>Email:</label>
              <input type= "email" name="email" class="form-control col-sm-12 form-custom-style" placeholder="Enter email">
            </div>
            <div class="form-group">
              <label>Message:</label>
              <textarea name="message" class="form-control col-sm-12 form-custom-style" id="textEditor" placeholder="Enter your message"></textarea>
            </div>
            <div class="form-group">
              <button type="submit" name="" class="btn btn-custom-primary submit col-sm-4">Submit
              </button>
            </div>
          </form>