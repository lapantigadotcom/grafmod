@extends('theme.kks212sby.main')
@section('content')
  <title>Grand Shamaya | News & Event</title>
@section('content')
 <section class="box overlay white-transparant-background experience" style="margin-bottom:0; height:100%">

<div class="main-page">
<div id="artikel" class="modal fade">
        <div class="modal-dialog">
          <div class="modal-content">
          </div>
        </div>
      </div>
 <!-- top-nav -->
 <div class="top-nav">
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-sm-6"></div>
      <div class="col-md-6 col-sm-6"></div>
    </div>
  </div>   
</div>

<!-- main-banner -->
<div class="main-banner">
  <div class="container">
  </div>
</div>
<!-- post-widgets -->
<div class="post-widgets">
  <div class="container">
    <div class="row">
      <div class="col-md-8 ">

    
   <!-- Start Contact Form -->
<div id="contact-form" class="contatct-form">
 
                     <h7 style="color:red">* Required Field</h7>
                     <br>
              @include('page.partials.notification')
              @if (count($errors) > 0)
              <div class="alert alert-danger">
                <strong>Whoops!</strong> Form yang anda isi belum lengkap.<br><br>
                <ul>
                  @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                  @endforeach
                </ul>
              </div>
              @endif
               {!! Form::open(['route'=>'home.doContact', 'method' => 'POST','files' => true]) !!}              
                @include('theme.kks212sby.form_contact',['buttonSubmit' => 'SUBMIT'])
              {!! Form::close() !!}
              </div> 
                        <!-- End Contact Form -->
 


      </div> 

         <!-- sidebar -->
         <div class="col-md-4">
           @include('theme.kks212sby.sidebar_blog')
         </div>
         <!-- /sidebar -->
    </div>        
  </div>             
</div>                 

</div> 
   
 </section> 
 
@endsection
 