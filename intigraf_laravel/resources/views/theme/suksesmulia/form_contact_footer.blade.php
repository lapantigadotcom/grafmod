          <!-- Appilication Form Start-->
          <div class="col-sm-4 col-sm-offset-4 reveal">
            <h3 style="font-family:Playfair Display">Stay in Touch</h3>
            {{ $errors->first() }}
            {!! Form::open(array('route' => 'home.adukan', 'method' => 'post', 'class'=>'', 'id' =>'reservation_form')) !!}
             <div class="form-group">
              <label>Name:</label>
              <input type= "text" id="reservation_name" name="nama_lengkap" class="form-control" placeholder="Enter name">
            </div>

            <div class="form-group">
              <label>Email:</label>
              <input type= "email" id="reservation_email" name="email" class="form-control" placeholder="Enter email">
            </div>

            <div class="form-group">
              <label>Message:</label>
              <textarea class="form-control" placeholder="Enter your message" name="isi" id="form_message"></textarea>
            </div>

            <div class="form-group">
              <button type="submit" class="btn btn-custom-primary submit col-sm-4" data-loading-text="Please wait...">Submit
              </button>
            </div> 
             {!! Form::close() !!}
            </div>
          <!-- Application Form End--> 