 <section id="services" class="divider parallax bg-biru overlay-dark-6" data-parallax-ratio="0.1" >
      <div class="container pt-60 pb-60">
        <div class="section-title text-center">
          <div class="row">
            <div class="col-md-8 col-md-offset-2">
              <h2 class="text-uppercase title text-white">KKS 212 <span class="text-hijau font-weight-400"> Produk</span></h2>
              <p class="text-uppercase letter-space-4 text-gray-lightgray">Berjamaah, Amanah, Izzah</p>
            </div>
          </div>
        </div>
        <div class="section-content  col-md-12">
          <div class="row">
            <div class="owl-carousel-4col">
              <div class="item ">
                <div class="service-block bg-white">
                  <div class="content text-left flip p-25 pt-0">
                    <h4 class="line-bottom mb-10">Belanja Halal</h4>
                    <p>Bagi konsumen, 212Mart adalah tempat belanja halal. Apa lagi yang lebih baik daripada konsumsi yang halal bagi kaum Muslim?...</p>
                    <a class="btn btn-dark bg-hijau btn-sm text-uppercase mt-10" href="#">Selengkapnya</a>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="service-block mb-md-30 bg-white">
                   <div class="content text-left flip p-25 pt-0">
                     <h4 class="line-bottom mb-10">Grosiran</h4>
                    <p>Bagi warung sekitar, 212Mart adalah agen atau grosir produk yang mereka jual. Supaya tidak mematikan warung tetangga, ...</p>
                    <a class="btn btn-dark bg-hijau btn-sm text-uppercase mt-10" href="#">Selengkapnya</a>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="service-block mb-md-30 bg-white">
                   <div class="content text-left flip p-25 pt-0">
                     <h4 class="line-bottom mb-10">Hasil Usaha</h4>
                    <p>Bagi investor, 212Mart adalah salah satu instrumen investasi. Ikut memiliki 212Mart secara berjamaah ...</p>
                    <a class="btn btn-dark bg-hijau btn-sm text-uppercase mt-10" href="#">Selengkapnya</a>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="service-block mb-md-30 bg-white">
                   <div class="content text-left flip p-25 pt-0">
                     <h4 class="line-bottom mb-10">Sedekah</h4>
                    <p>Bagi kaum dhuafa, 212Mart adalah wahana pengumpul sedekah lain. Keuntungan yang dihasilkan oleh 212Mart...</p>
                    <a class="btn btn-dark bg-hijau btn-sm text-uppercase mt-10" href="#">Selengkapnya</a>
                  </div>
                </div>
              </div>
               
            </div>
          </div>
        </div>
      </div>
    </section>