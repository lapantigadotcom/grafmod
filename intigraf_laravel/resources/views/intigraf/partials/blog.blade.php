    <div class="news" style="background: url(assets/tema/lapantiga/images/bg-news-front.jpg)">

      <div class="container">

        <div class="row">
         <?php
         $counter = 0;
         ?>
         @foreach($data['section_3']->post()->orderBy('created_at', 'desc')->limit(2)->get() as $d)
         <div class="col-sm-4 margin-bottom-30" id="blogthumb">

           <div class="widget_pw_latest_news">
            <a class="latest-news" href="{{ url('/news/'. $d->id) }}">
              <div class="latest-news__date">
                <div class="latest-news__date__month">{{ date("M" ,strtotime($d->created_at)) }} </div>
                <div class="latest-news__date__day">{{ date("d" ,strtotime($d->created_at)) }} </div>
              </div>

              @if (isset($d->mediaPost->file))
              <img  style="left:none" style="max-width: 258px" src="{{asset('upload/media/'.$d->mediaPost->file)}}" alt="" class="wp-post-image" > 
              @else
              <img  style="left:none" src="{{asset('assets/tema/intigrafika/images/no-image.jpg')}}" alt="" class="wp-post-image" > 
              @endif

              <div class="latest-news__content">
                <h4 class="latest-news__title">{!! substr(strip_tags($d->title), 0, 30).'...' !!}</h4>
                <div class="latest-news__author">{{ $d->user->name }} | {{ date("d M Y" ,strtotime($d->created_at)) }}</div>
              </div>
            </a>
          </div>


        </div>
        @endforeach
        <div class="col-sm-4 margin-bottom-30">
         <?php
         $counter = 0;
         ?>
         @foreach($data['section_3']->post()->orderBy('created_at', 'desc')->limit(3)->get() as $d)
         <div class="widget_pw_latest_news">
           <a class="latest-news  latest-news--inline" href="{{ url('/news/'. $d->id) }}">
            <div class="latest-news__content">
              <h4 class="latest-news__title">{!! substr(strip_tags($d->title), 0, 30).'...' !!}</h4>
              <div class="latest-news__author">{{ $d->user->name }} | <b> {{ date("d M Y" ,strtotime($d->created_at)) }} </b></div>
            </div>
            @endforeach
          </a>
          <a class="latest-news  latest-news--more-news" href="{{ url('/news/'. $d->id) }}">
            Latest Articles
          </a>
        </div>          
      </div>

    </div>

  </div>

</div> 
</div> 