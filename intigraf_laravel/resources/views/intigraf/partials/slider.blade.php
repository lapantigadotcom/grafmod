    <div class="jumbotron jumbotron--with-captions">
      
      <div data-interval="5000" data-ride="carousel" id="headerCarousel" class="carousel slide">
 
        <div class="carousel-inner">
          

          
            <div class="item">
            <img alt="PHP Framework" sizes="100vw" srcset="assets/tema/intigrafika/images/slider/slider2.jpg" src="assets/tema/intigrafika/images/slider/slider2.jpg">
            <div class="container">
              <div class="jumbotron-content">
                <div class="jumbotron-content__title">
                </div>
                <div class="jumbotron-content__description ">
                                <h1>Cetak Kualitas Prima</h1>

                  <p class="p1">
                    <span class="s1">Cetak agenda, buku, katalog, stiker, kalendar dll</span>
                  </p>
                  
                  <div class="w69b-screencastify-mouse"></div>
                </div>
              </div>
            </div>
          </div><!-- /.item -->

          <div class="item active">
            <img alt="PHP Framework" sizes="100vw" srcset="assets/tema/intigrafika/images/slider/slider1.jpg" src="assets/tema/intigrafika/images/slider/slider1.jpg">
            <div class="container">
              <div class="jumbotron-content">
                <div class="jumbotron-content__title">
                </div>
                <div class="jumbotron-content__description">
                                <h1>Souvenir</h1>

                  <p class="p1">
                    <span class="s1">Kebutuhan souvenir perusahaan: payung, ballpoint, flashdisk dll</span>
                  </p>
                  <a target="_self" href="#" class="btn btn-primary">Get Now</a>

                  <div class="w69b-screencastify-mouse"></div>
                </div>
              </div>
            </div>
          </div><!-- /.item -->
          
        </div> 
      
        <div class="container">
 
          <a data-slide="prev" role="button" href="#headerCarousel" class="left jumbotron__control">
            <i class="fa  fa-caret-left"></i>
          </a>
          <a data-slide="next" role="button" href="#headerCarousel" class="right jumbotron__control">
            <i class="fa  fa-caret-right"></i>
          </a>
        </div> 

      </div> 
      
    </div> 