 
 		<div class="top warna-inti ">

 			<div class="container ">

 				<div class="top__tagline">CV. Intigrafika Sukses Mulia | Percetakan</div>

 				<nav class="top__menu hidden-xs">
 					<ul class="top-navigation js-dropdown">
 						<li>
 							<a href="https://plus.google.com/+csintigrafika?prsrc=5" target="_blank"><i class="fa fa-google-plus"></i>@intigrafika</a>

 						</li>
 						<li>
 							<a href="https:facebook.com" target="_blank"><i class="fa fa-facebook"></i> intigrafika</a>
 						</li>
 						<li>
 							<i class="fa fa-cogs"></i> Mesin Cetak
 						</li>
 					</ul> 
 				</nav>

 			</div> 
      </div> 

 			
<div id="flipkart-navbar">
    <div class="container">
        <div class="row row1 text-inti">
            <ul class="largenav pull-right">
                <li class="upper-links"><a class="links" href="#">Home</a></li>
                <li class="upper-links"><a class="links" href="#">Produk</a></li>
                <li class="upper-links"><a class="links" href="#">Bisnis</a></li>
                <li class="upper-links"><a class="links" href="#">Jasa</a></li>
                <li class="upper-links"><a class="links" href="#">Klien</a></li>
                <li class="upper-links"><a class="links" href="#">Hubungi Kami</a></li>
                
                <li class="upper-links dropdown"><a class="links" href="#">Blogs</a>
                    <ul class="dropdown-menu">
                        <li class="profile-li"><a class="profile-links" href="#">Informasi</a></li>
                        <li class="profile-li"><a class="profile-links" href="#">Tips Cetak</a></li>
                        <li class="profile-li"><a class="profile-links" href="#">Mesin Cetak</a></li>
                    </ul>
                </li>
            </ul>
        </div>
        <div class="row row2">
            <div class="col-sm-2">
                <h2 style="margin:0px;"><span class="smallnav menu" onclick="openNav()">☰ Brand</span></h2>
               <a href="{{url ('/')}}">
        <img class="img-responsive" srcset="{{asset('/assets/tema/intigrafika/images/logo_intigrafika.png')}}" alt="logo intigrafika" src="{{asset('/assets/tema/intigrafika/images/logo_intigrafika.png')}}">
      </a>
            </div>
            <div class="flipkart-navbar-search smallsearch col-sm-8 col-xs-11">
                <div class="row">
                    <input class="flipkart-navbar-input col-xs-11" type="" placeholder="Cari artikel, produk, info lain..." name="">
                    <button class="flipkart-navbar-button col-xs-1">
                        <svg width="15px" height="15px">
                            <path d="M11.618 9.897l4.224 4.212c.092.09.1.23.02.312l-1.464 1.46c-.08.08-.222.072-.314-.02L9.868 11.66M6.486 10.9c-2.42 0-4.38-1.955-4.38-4.367 0-2.413 1.96-4.37 4.38-4.37s4.38 1.957 4.38 4.37c0 2.412-1.96 4.368-4.38 4.368m0-10.834C2.904.066 0 2.96 0 6.533 0 10.105 2.904 13 6.486 13s6.487-2.895 6.487-6.467c0-3.572-2.905-6.467-6.487-6.467 "></path>
                        </svg>
                    </button>
                </div>
            </div>
            <div class="cart largenav col-sm-2">
                <a class="cart-button">
                    <i class="fa fa-map-marker text-white"></i>
                    <span class="item-number ">Lokasi</span>
                </a>
            </div>
        </div>
    </div>
</div>
<div id="mySidenav" class="sidenav">
    <div class="container" style="background-color: #2874f0; padding-top: 10px;">
        <span class="sidenav-heading">Home</span>
        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">×</a>
    </div>
   <a class="links" href="#">Home</a>
   <a class="links" href="#">Produk</a>
</div>
 	 