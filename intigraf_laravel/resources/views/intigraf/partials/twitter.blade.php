

<div class="values" style="background:#444;color:#fff;padding: 18px">

 <div class="container">

    <div class="col-sm-8 col-sm-offset-2">

        <div class="owl-carousel">
            <?php
            $counter = 0;
            ?>
            @foreach($data['section_3']->post()->orderBy('created_at', 'desc')->limit(2)->get() as $d)
            <center>
             <div class="widget_pw_latest_news">
                
                <h4 class="latest-news__title text-white">{!! substr(strip_tags($d->title), 0, 60).'...' !!}</h4>
                <div class="latest-news__author">{{ $d->user->name }} | {{ date("d M Y" ,strtotime($d->created_at)) }}</div>
            </a>
        </div> 
    </center>
    @endforeach
</div>

</div>
</div>
</div>