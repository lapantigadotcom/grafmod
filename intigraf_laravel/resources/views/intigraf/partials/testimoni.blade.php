  <section id="experts" class="divider parallax overlay-dark-4" data-parallax-ratio="0.1" data-bg-img="{{asset ('assets/tema/212mart_sby/images/slider/20170924_132812.jpg')}}">
      <div class="container pt-150 pb-150">
        <div class="section-title text-center">
          <div class="row">
            <div class="col-md-8 col-md-offset-2">
              <h2 class="text-uppercase title text-biru">Testimoni </h2>
              <p class="text-uppercase letter-space-4 text-hijau">Kata mereka tentang Koperasi Syariah 212</p>
            </div>
          </div>
        </div>
        <div class="row mtli-row-clearfix">
          <div class="col-md-12">
            <div class="owl-carousel-4col">@foreach($data['testi'] as $tes)
              <div class="item">
                <div class="team-members maxwidth400">
                  <div class="team-thumb">
                    <img class="img-fullwidth" alt="{{$tes->name}} " src="{{asset('upload/media/'.$tes->file)}}" >

                  </div>
                  <div class="team-bottom-part border-bottom-theme-color-2-2px border-1px bg-white text-center p-10 pt-20 pb-10">
                    <h4 class="text-uppercase font-raleway text-theme-color-2 font-weight-600 line-bottom-center m-0">{{$tes->name}} </h4>
                    <h5 class="m-0">- {{$tes->jabatan}} -</h5>
                    <p class="font-13 mt-10 mb-10">“{{$tes->isi}}” </p>
                    <ul class="styled-icons icon-sm icon-gray icon-hover-theme-colored">
                     </ul>
                  </div>
                </div>
              </div> @endforeach
            </div>
          </div>
        </div>
      </div>
    </section>