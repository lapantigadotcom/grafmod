<br><br>
<div class="container">
			
			<div class="row jumbotron-overlap first" style="margin-bottom:15px">


				<div class="col-sm-12" id="workareaawal">

					<h3 class="widget-title big lined">
						<span>Workarea Kami</span>
					</h3>
					
				</div>
				
			</div>
			
			<div class="row">
				
				<div class="col-sm-4">

					<div class="widget_pw_icon_box margin-bottom-30">
						<a target="_self" href="#" class="icon-box">
							<i class="fa fa-book" style="color:#f90"></i>
							<h4 class="icon-box__title">Cetak Agenda</h4>
							<span class="icon-box__subtitle">Memulai dan menyimpan ide dalam bentuk tulisan sangatlah perlu apalagi dengan agenda yang ekslusif lebih kita percaya diri.</span>
						</a>
					</div>
					
					<div class="widget_pw_icon_box margin-bottom-30">
						<a target="_self" href="#" class="icon-box">
							<i class="fa fa-calendar" style="color:red"></i>
							<h4 class="icon-box__title">Cetak Kalendar</h4>
							<span class="icon-box__subtitle">Sarana untuk mengatur jadwal kita biar tidak lepas dari aturan sekaligus perencanaan</span>
						</a>
					</div>
					
				</div>
				
				<div class="col-sm-4">
					
					<div class="widget_pw_icon_box margin-bottom-30">
						<a target="_self" href="#" class="icon-box">
							<i class="fa fa-sticky-note" style="color:#01A6BA"></i>
							<h4 class="icon-box__title">Cetak Stiker</h4>
							<span class="icon-box__subtitle">Alat promosi untuk outdoor dan menjadi strategi untuk mengenalkan produk  </span>
						</a>
					</div>
					
					<div class="widget widget_pw_icon_box margin-bottom-30">
						<a target="_self" href="#" class="icon-box">
							<i class="fa fa-table" style="color:#93C24F"></i>
							<h4 class="icon-box__title">Cetak Katalog</h4>
							<span class="icon-box__subtitle">Merupakan sarana penunjang promosi yang berupa daftar koleksi produk. </span>
						</a>
					</div>
					
				</div>

				<div class="col-sm-4">
					
					<div class="widget_pw_icon_box margin-bottom-30">
						<a target="_self" href="#" class="icon-box">
							<i class="fa fa-file-image-o" style="color:#253E6A"></i>
							<h4 class="icon-box__title">Cetak Brosur/Flyer</h4>
							<span class="icon-box__subtitle">Alat promosi / marketing yang sangat membantu mengenalkan produk Anda</span>
						</a>
					</div>
					
					<div class="widget_pw_icon_box margin-bottom-30">
						<a target="_self" href="#" class="icon-box">
							<i class="fa fa-shopping-bag" style="color:#444"></i>
							<h4 class="icon-box__title">Cetak Paperbag</h4>
							<span class="icon-box__subtitle">Disamping sebagai tempat barang bawaan, paperbag ini jika di design dengan apik akan punya nilai tersendiri</span>
						</a>
					</div>
					
				</div>

			</div>

		</div>