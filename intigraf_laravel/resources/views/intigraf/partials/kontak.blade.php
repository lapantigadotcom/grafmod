<div class="container" style="margin-bottom: 40px">
   <section class="divider">
      <div class="container pt-20">
         
        <div class="row pt-10">
          <div class="col-md-5">
          <h4 class="mt-0 mb-30 line-bottom">Lokasi Workshop </h4>
         
          <div 
            data-address="Purimas Regency Blok B-3 No 57 Gunung Anyar Surabaya Jawa Timur, Indonesia "
            data-popupstring-id="#popupstring1"
            class="map-canvas autoload-map"
            data-mapstyle="212map"
            data-height="420"
            data-latlng="-7.3353332,112.7816617"
            data-title="212 Mart"
            data-zoom="15"
            data-marker="images/map_marker212.png">   
          </div>
          <div class="map-popupstring hidden" id="popupstring1">
            <div class="text-center">
              <h3>Workshop</h3>
              <p>Jalan Kapas Madya 1F No.97-99, Gading, Tambaksari, Kota SBY, Jawa Timur 60134 </p>
            </div>
          </div>
           
          <script src="//maps.google.com/maps/api/js?key=AIzaSyDlS_YgMMNGsQkQbVijCfPcH4EtKwcdxoo"></script>
                <script src="{{asset('assets/tema/intigrafika/js/google-map-init.js')}}"></script>

          </div>
          
          @include('intigraf.partials.formcontact')
        </div>
      </div>
    </section>
  </div>