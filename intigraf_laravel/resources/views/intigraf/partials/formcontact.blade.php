<div class="col-md-7">
            <h4 class="mt-0 mb-30 line-bottom">Hubungi Kami</h4>
            <!-- Contact Form -->
               {{ $errors->first() }}
               <h5 class="text-hijau">{{ Session::get('success') }}</h5>
            {!! Form::open(array('route' => 'home.adukan', 'method' => 'post', 'class'=>'', 'id' =>'reservation_form')) !!}

            <form>

              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="form_name">Nama <small>*</small></label>
                     <input type= "text"  name="nama_lengkap" class="form-control" placeholder="Nama Anda" required="">
                   </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label>Email <small>*</small></label>
                     <input type= "email" name="email" class="form-control required email" placeholder="Email">

                   </div>
                </div>
              </div>                
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="form_name">Topik <small>*</small></label>
                    <input name="form_subject" class="form-control required" type="text" placeholder="Topik">
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="form_phone">Telpon</label>
                    <input name="form_phone" class="form-control" type="text" placeholder="No. HP">
                  </div>
                </div>
              </div>

              <div class="form-group">
                <label for="form_name">Pesan</label>
                <textarea class="form-control required" placeholder="Tuliskan disini" name="isi" ></textarea>               </div>
              <div class="form-group">
                <input name="form_botcheck" class="form-control" type="hidden" value="" />
                <button type="submit" class="btn btn-flat bg-biru text-white text-uppercase mt-10 mb-sm-30 border-left-theme-color-2-4px" data-loading-text="Please wait...">Kirim</button>
                <button type="reset" class="btn btn-flat  bg-hijau text-white text-uppercase mt-10 mb-sm-30 border-left-theme-color-2-4px">Hapus</button>
              </div>
            </form>
             {!! Form::close() !!}
            
          </div>