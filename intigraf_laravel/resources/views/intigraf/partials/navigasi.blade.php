 
<div class="row">

  <header class="header col-md-12" id="header">
 

  <div class="header__widgets">

    <div class="widget-icon-box">

      <div class="icon-box">  
        <i class="fa fa-headphones"></i>
        <h4 class="icon-box__title">Tlp/Fax</h4>
        <span class="icon-box__subtitle">031-3729953</span>
      </div>

    </div>

    <div class="widget-icon-box">

      <div class="icon-box">  
        <i class="fa fa-instagram"></i>
        <h4 class="icon-box__title">Instagram</h4>
        <span class="icon-box__subtitle">intigrafika</span>
      </div>

    </div>

    <div class="widget-icon-box">

      <div class="icon-box">
        <i class="fa fa-envelope-o"></i>
        <h4 class="icon-box__title">Email Us</h4>
        <span class="icon-box__subtitle">csintigrafika@gmail.com</span>
      </div>

    </div>

    <div class="widget-icon-box">

      <div class="icon-box">
        <i class="fa fa-home"></i>
        <h4 class="icon-box__title">Workshop</h4>
        <span class="icon-box__subtitle">Kapas Madya 1F No.97-99 Surabaya</span>
      </div>

    </div>

     <div class="widget-icon-box">

      <div class="icon-box">
        <i class="fa fa-whatsapp"></i>
        <h4 class="icon-box__title">Whatsapp</h4>
        <span class="icon-box__subtitle">081-232-299-53</span>
      </div>

    </div>

  </div> 

</header>

</div>

