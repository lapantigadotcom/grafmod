
		<div class="counters" style="background: url(assets/tema/intigrafika/images/bg_footer.jpg ) no-repeat cover  center">
			
			<div class="container">
			
			<div class="row margin-bottom-60">
				
				<div class="col-sm-12">
					
					<div class="widget_text">
						
						<h3 class="widget-title lined big" >
							<span>Klien Kami</span>
						</h3>
						<div class="logo-panel" style="background-color: rgba(255, 255, 255, 0.9);">
							<div class="row owl-carousel" id="logoklien" > 
							<div class="item">
									<a href="#"><img alt="Client" title="Mandiri" src="assets/tema/intigrafika/images/klien/mandiri.jpg"></a>
								</div>
								<div class="item">
									<a href="#"><img alt="Client" title="Badak" src="assets/tema/intigrafika/images/klien/pjb.jpg"></a>
								</div>
							<div class="item">
									<a href="#"><img alt="Client" title="Badak" src="assets/tema/intigrafika/images/klien/badak.jpg"></a>
								</div>
			<div class="item" >
									<a href="#"><img alt="Client" title="KPU"  src="assets/tema/intigrafika/images/klien/kpu.jpg"></a>
								</div> 

								<div class="item" >
									<a href="#"><img alt="Client" title="Ibis"  src="assets/tema/intigrafika/images/klien/ibis.jpg"></a>
								</div>

									<div class="item">
									<a href="#"><img alt="Client" title="Kotim" src="assets/tema/intigrafika/images/klien/kotim.jpg"></a>
								</div>

								<div class="item" >
									<a href="#"><img alt="Client" title="Surabaya"  src="assets/tema/intigrafika/images/klien/sby.jpg"></a>
								</div>

								<div class="item" >
									<a href="#"><img alt="Client" title="Unsrat"  src="assets/tema/intigrafika/images/klien/unsrat.jpg"></a>
								</div> 
							</div>
						</div>

					</div>
					
				</div>
				
			</div>			 

			<br>


				 
				 <div  class="widget_pw_number-counter panel-first-child panel-last-child">
					 <div data-speed="1000" class="widget-number-counters">
						 <div class="number-counter">
							 <i class="number-counter__icon fa fa-clock-o"></i>
							 <div data-to="7" class="number-counter__number js-number">00</div>
							 <div class="number-counter__title">Tahun di Percetakan</div>
				 		 </div>
				 		 <div class="number-counter">
							 <i class="number-counter__icon fa fa-users"></i>
				 	 		 <div data-to="186" class="number-counter__number js-number">00</div>
				 	 		 <div class="number-counter__title">Klien </div>
				 		 </div>
				 		 <div class="number-counter">
							 <i class="number-counter__icon fa fa-globe"></i>
					   		 <div data-to="230" class="number-counter__number js-number">00</div>
				 	 		 <div class="number-counter__title">Proyek Cetak</div>
				 		 </div>
				 		 <div class="number-counter">
							 <i class="number-counter__icon fa fa-send"></i>
				 	   		 <div data-to="50" class="number-counter__number js-number">00</div>
				 	   		 <div class="number-counter__title">Lbr Kertas (Hari)</div>
						 </div>
					 </div>
				 </div>

			</div>
			
		</div> </div>