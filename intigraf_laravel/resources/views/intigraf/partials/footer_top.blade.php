		<footer class="footer">
			
			<div class="footer-top">
				
				<div class="container">
					
					<div class="row">

						<div class="col-xs-12 col-md-4">
							
							<p>
								<img alt="logo-footer intigrafika" src="{{asset ('assets/tema/intigrafika/images/logo_white_intigrafika.png')}}" class="logo_white_intigrafika">
							</p>
							<p>
								Intigrafika - Percetakan majalah,  percetakan kalender, percetakan brosur,  percetakan stiker , percetakan agenda. Percetakan dengan kualitas Istimewa dan tepat waktu.
							</p>





						</div>

						<div class="col-xs-12 col-md-2">
							
							<div class="widget_nav_menu">
								
								<h6 class="footer-top__headings">About</h6>
								<ul>
									<li><a href="#workareaawal">Workshop</a></li>
									<li><a href="[[~95]]">Hubungi Kami</a></li>
									<li><a href="[[~67]]">Portfolio</a></li>

								</ul>
								
							</div>
							
						</div>
						<div class="col-xs-12 col-md-2">
							
							<div class="widget_nav_menu">
								
								<h6 class="footer-top__headings">Our Blogs</h6>
								<ul>
									<li><a href="[[~38]]">Artikel</a></li>
									<li><a href="[[~22]]">Mesin Cetak</a></li>
									<li><a href="[[~23]]">Tips Cetak</a></li><br>
									 
								</ul>

							</div>
							
						</div>
						
						<div class="col-xs-12 col-md-4">
							
							<h6 class="footer-top__headings">Office & Workshop</h6>

							<p>		Jalan Kapas Madya 1F No.97-99, Gading, Tambaksari, Kota SBY, Jawa Timur 60134
								<br>
								Phone/Fax: 031-3729953<br>
								<i class="fa fa-whatsapp"></i> &nbsp;: 081-232-299-53</p>
								<p>
									<a data-toggle="modal" data-target="#twitter"  class="btn btn-info">Follow Us</a>		

								</p>

							</div>

						</div>

					</div>

				</div>

<div class="footer-bottom">
	
	<div class="container">
		
		<div class="footer-bottom__left" style="color:#fff">
			email: csintigrafika@gmail.com

		</div>
		<div class="footer-bottom__right" style="color:#fff">
			Copyright &copy; 2010 - 2017 CV. Intigrafika Sukses Mulia. All rights reserved. 




		</div>
		
	</div>
	
</div>

</footer>  