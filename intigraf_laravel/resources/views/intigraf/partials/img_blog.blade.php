   <section id="home" class="divider parallax fullscreen" data-parallax-ratio="0.1" data-bg-img="{{asset ('assets/tema/212mart_sby/images/slider/20170924_132812.jpg')}}" style="max-height: 350px;" >
      <div class="display-table">
        <div class="display-table-cell">
          <div class="container pt-150 pb-150">
            <div class="row">
              <div class="col-md-8 col-md-offset-2 text-center">
                <div class="pb-10 pt-10">
                   <h1 class="text-uppercase text-white mt-0 inline-block bg-theme-colored-transparent border-left-theme-color-2-6px border-right-theme-color-2-6px pl-40 pr-40 pt-5 pb-5 font-32">@yield('meta_title')</h1> 
                 </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section> 