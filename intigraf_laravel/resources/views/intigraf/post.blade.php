 

@section('meta_title', 'Event & News')
@section('og-image')
<meta property="og:image" content="{{asset ('assets/tema/212mart_sby/images/slider/bg_service.jpg')}}" />
@endsection
@section('meta_desc')
<meta name="description" content=""/> 
@endsection
@section('meta_key')
<meta name="keywords" content="grandshamaya, pp property, grand shamaya surabaya, pt pp tbk, grand shamaya"/>
@endsection
@extends('theme.kks212sby.main')
@section('content')   
<!-- Section: Blog -->
    <section>
      <div class="container mt-30 mb-30 pt-30 pb-30">
       
        <div class="row">
          <div class="col-md-3">
            <div class="sidebar sidebar-left mt-sm-30">
               
              <div class="widget">
                <h5 class="widget-title line-bottom">Twitter Feed</h5>
                <div class="twitter-feed list-border clearfix" data-username="Envato" data-count="3"></div>
              </div>
               
              <div class="widget">
                <h5 class="widget-title line-bottom">Tags</h5>
                <div class="tags">
                  @foreach($data['content']->tag as $row)
                   <a href=""> {!! $row->title !!}</a>
                    @endforeach
                   
                </div>
              </div>
            </div>
          </div>
          @if(!empty($data['content']))
<div class="col-md-6">
  <div class="blog-posts single-post">
    <article class="post clearfix mb-0">
      <div class="entry-header">
        <div class="post-thumb thumb">  @if(isset($data['content']->mediaPost->file))
          <img src="{{ asset('upload/media/'.$data['content']->mediaPost->file) }}" class="img-responsive">
        @endif</div>
      </div>
      <div class="entry-content">
        <div class="entry-meta media no-bg no-border mt-15 pb-20">
          <div class="entry-date media-left text-center flip bg-theme-colored pt-5 pr-15 pb-5 pl-15">
            <ul>
              <li class="font-16 text-white font-weight-600">{{ date("d" ,strtotime($data['content']->created_at)) }}</li>
              <li class="font-12 text-white text-uppercase">{{ date("M" ,strtotime($data['content']->created_at)) }}</li>
            </ul>
          </div>
          <div class="media-body pl-15">
            <div class="event-content pull-left flip">
              <h3 class="entry-title text-biru text-uppercase pt-0 mt-0"><a href="#">@if ($data['english'])
        {!! $data['content']->title_eng !!}
        @else
        {!! $data['content']->title !!}
        @endif</a></h3> 
              <span class="mb-10 text-gray-darkgray mr-10 font-13"><i class="fa fa-user mr-5 text-theme-colored"></i>  {{ $data['content']->user->name }}</span>
             
            </div>
          </div>
        </div>
        <p class="mb-15">
          @if ($data['english'])
          {!! $data['content']->eng_description !!}
          @else
          {!! $data['content']->description !!}
          @endif
        </p>
        
      </div>
    </article>
    <div class="tagline p-0 pt-20 mt-5">
      <div class="row">
        <div class="col-md-8">
          <div class="tags">
            <p class="mb-0"><i class="fa fa-tags text-theme-colored"></i> <span>Tags:</span> @foreach($data['content']->tag as $row)
                    <a href="#">{!! $row->title !!}</a>,
                    @endforeach</p>
          </div>
        </div>
        <div class="col-md-4">
          <div class="share text-right flip">
            <p><i class="fa fa-share-alt text-theme-colored"></i> Share</p>
          </div>
        </div>
      </div>
    </div>
    
  </div>
</div>
@endif
          <div class="col-md-3">
            <div class="sidebar sidebar-right mt-sm-30">
              <div class="widget">
                <h5 class="widget-title line-bottom">Cari Artikel</h5>
                <div class="search-form">
                  <form>
                    <div class="input-group">
                      <input type="text" placeholder="Cari Artikel lain" class="form-control search-input">
                      <span class="input-group-btn">
                      <button type="submit" class="btn search-button"><i class="fa fa-search"></i></button>
                      </span>
                    </div>
                  </form>
                </div>
              </div>
              <div class="widget">
                <h5 class="widget-title line-bottom">Kategori</h5>
                <div class="categories">
                  <ul class="list list-border angle-double-right">
                    <li><a href="#">Event<span>(19)</span></a></li>
                    <li><a href="#">Informasi<span>(21)</span></a></li>
                    <li><a href="#">Berita<span>(15)</span></a></li>
                  </ul>
                </div>
              </div>
              <div class="widget">
                <h5 class="widget-title line-bottom">Informasi Terbaru</h5>
                <div class="latest-posts">
                            @include('theme.kks212sby.sidebar_blog')
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
@endsection