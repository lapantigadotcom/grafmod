<!DOCTYPE html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>Intigrafika - Percetakan majalah,  percetakan kalender, percetakan brosur,  percetakan stiker , percetakan agenda Percetakan</title>
  <meta name="description" content="percetakan, kalender , brosur, cetak brosur , cetak tabloid, cetak undangan ,cetak Poster, cetak baleho, mesin cetak, oliver, sakurai, mesin potong, kaos, sablon,  cetak map, paperbag, brosur, splier mesin cetak, kataloge, baju promotion, pernikahan, contoh brosur, cetak brosur, brosur, design brosur, kartu undangan, desain brosur, cetak katalog, percetakan kalender, desain lebaran, percetakan undangan, advertising  kalender, cetak stiker l,pilkada, promotion, undangan pernikahan, percetakan surabaya, percetakan brosur Surabaya , bisnis percetakan, katalog undangan pernikahan, desain spanduk, cetak stiker, Surabaya, offset, mesin cetak, cetak mesin, cetak baleho,  percetakan buku agenda,c ompany profile, , daftar supplier mesin cetak, , proses pencetakan brosur, kalender meja, promosi, benhard fashion, percetakan digital, intigrafika, percetakanku, usaha cetak, desain kartu undangan, jenis kartu undangan, contoh gambar spanduk, stiker, leaflet, kop surat, percetakan kartu undangan"/> 

  <meta name="keywords" content="Cetak majalah dan cetak kalender, Kata Pengantar, Percetakan majalah,  percetakan kalender, percetakan brosur,  percetakan stiker , percetakan agenda Percetakan, percetakan, Percetakan majalah,  Percetakan kalender,percetakan brosur,  percetakan stiker , Percetakan agenda ,percetakan tabloid, percetakan undangan ,cetak Poster, cetak baleho, mesin cetak, oliver, sakurai, mesin potong, kaos, sablon,  cetak map, paperbag, brosur, splier mesin cetak, kataloge, baju promotion, pernikahan, contoh brosur, cetak brosur, brosur, design brosur, kartu undangan, desain brosur, cetak katalog, percetakan kalender, desain lebaran, percetakan undangan, advertising  kalender, cetak stiker l,pilkada, promotion, undangan pernikahan, percetakan surabaya, percetakan brosur Surabaya , bisnis percetakan, katalog undangan pernikahan, desain spanduk, cetak stiker, Surabaya, offset, mesin cetak, cetak mesin, cetak baleho,  percetakan buku agenda,c ompany profile, , daftar supplier mesin cetak, , proses pencetakan brosur, kalender meja, promosi, percetakan digital, intigrafika, percetakanku, usaha cetak, desain kartu undangan, jenis kartu undangan, contoh gambar spanduk, stiker, leaflet, kop surat, percetakan terbaik, percetakan murah, percetakan cepat"/>
  <meta name="viewport" content="width=device-width">
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
  <link href="{{asset('assets/tema/intigrafika/css/style.css')}}" rel="stylesheet">
  <link href="{{asset('assets/tema/intigrafika/css/grey.css')}}" rel="stylesheet">
  <link href="{{asset('assets/tema/intigrafika/css/navbar-inti.css')}}" rel="stylesheet">
  <link href="{{asset('assets/tema/intigrafika/css/magnific-popup.css')}}" rel="stylesheet">
  <link href="//fonts.googleapis.com/css?family=Roboto%3A400%2C700%7CSource+Sans+Pro%3A700%2C900&amp;subset=latin" rel="stylesheet">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.css" rel="stylesheet"/>

  <link rel="apple-touch-icon" sizes="57x57" href="assets/favicon/apple-touch-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="assets/favicon/apple-touch-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="assets/favicon/apple-touch-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="assets/favicon/apple-touch-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="assets/favicon/apple-touch-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="assets/favicon/apple-touch-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="assets/favicon/apple-touch-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="assets/favicon/apple-touch-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="assets/favicon/apple-touch-icon-180x180.png">
  <link rel="icon" type="image/png" href="assets/favicon/favicon-32x32.png" sizes="32x32">
  <link rel="icon" type="image/png" href="assets/favicon/android-chrome-192x192.png" sizes="192x192">
  <link rel="icon" type="image/png" href="assets/favicon/favicon-96x96.png" sizes="96x96">
  <link rel="icon" type="image/png" href="assets/favicon/favicon-16x16.png" sizes="16x16">
  <link rel="manifest" href="assets/favicon/manifest.json">
  <link rel="mask-icon" href="assets/favicon/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="msapplication-TileImage" content="assets/favicon/mstile-144x144.png">
  <meta name="theme-color" content="#ffffff">
  <meta property="og:title" content="Intigrafika - Percetakan majalah,  percetakan kalender, percetakan brosur,  percetakan stiker , percetakan agenda Percetakan"/>
  <meta property="og:type" content="news"/>
  <meta property="og:url" content="   "/>
  <meta property="og:image" content=" "/>
  <meta property="og:site_name" content="intigrafika.com"/>
  <meta property="fb:admins" content="intigrafika"/>
  <meta property="og:description" content="[[*introtext]] "/>
   <script src="{{asset('assets/tema/intigrafika/third-party/owl-carousel/owl.carousel.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('assets/tema/intigrafika/js/modernizr.custom.24530.js')}}" type="text/javascript"></script>
  <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
  <link href="{{asset('assets/tema/intigrafika/third-party/owl-carousel/owl.transitions.css')}}" rel="stylesheet">
 </head>
<body class="boxed">
  
    <div class="boxed-container">
  @include('intigraf.partials.top_nav')
   @include('intigraf.partials.navigasi')
   @include('intigraf.partials.slider')
  @include('intigraf.partials.services')
  @include('intigraf.partials.twitter')
  @include('intigraf.partials.cta')
  @include('intigraf.partials.blog')
  @include('intigraf.partials.kontak')
  @include('intigraf.partials.galeri')
  @include('intigraf.partials.klien')
  @include('intigraf.partials.footer_top')
</div>
  

  <script src="{{asset('assets/tema/intigrafika/js/bootstrap/carousel.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.js"></script>
<script src="{{asset('assets/tema/intigrafika/js/bootstrap/transition.js')}}"></script>
<script src="{{asset('assets/tema/intigrafika/js/bootstrap/button.js')}}"></script>
<script src="{{asset('assets/tema/intigrafika/js/bootstrap/collapse.js')}}"></script>
<script src="{{asset('assets/tema/intigrafika/js/bootstrap/validator.js')}}"></script>
<script src="{{asset('assets/tema/intigrafika/js/underscore.js')}}"></script>
<script src="{{asset('assets/tema/intigrafika/js/NumberCounter.js')}}"></script>
<script src="{{asset('assets/tema/intigrafika/js/jquery.magnific-popup.min.js')}}"></script>
<script src="{{asset('assets/tema/intigrafika/js/custom.js')}}"></script>

<script type="text/javascript">
$('#logoklien').owlCarousel({
    autoPlay: 3500,
  loop:true,
    margin:10,
    nav:false,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:5
        }
    }
});

$('.owl-carousel').owlCarousel({
      autoPlay: 4500,
  items:1,
    lazyLoad:true,
    loop:true,dots:false,dots: false,
    margin:10
});



</script>

<script type="text/javascript">

    $(document).ready(function() {
     
      $("#loadlazy").owlCarousel({
        items : 10,
        lazyLoad : true,dots:false,
      }); 
     
    });

</script>
  <script type="text/javascript">
function openNav() {
    document.getElementById("mySidenav").style.width = "70%";
    // document.getElementById("flipkart-navbar").style.width = "50%";
    document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.body.style.backgroundColor = "rgba(0,0,0,0)";
}
  </script>

  <script src="assets/tema/lapantiga/third-party/caption/jquery.caption.js"></script> 
 
  @yield('custom-js')
</body>
</html>