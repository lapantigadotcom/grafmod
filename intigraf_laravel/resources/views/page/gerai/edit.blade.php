@extends('app')

@section('content')
<section class="content-header">
	<h1>
		Dashboard
		<small>Control panel</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Dashboard</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">

	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">
						EDIT GERAI SIM
					</h3>                                    
				</div>
				<div class="box-body">
					{!! Form::model($data['content'],array('action' => ['GeraiController@update',$data['content']->id],'method' => 'PATCH')) !!}
					<div class="col-md-9">
              			@include('page.partials.notification')
						@include('page.errors.list')
	                  	
						@include('page.gerai.form-create',['submitText' => 'Perbarui'])
					</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</section>
@include('page.partials.media')
@endsection

@section('custom-head')

@stop
@section('custom-footer')

@stop
