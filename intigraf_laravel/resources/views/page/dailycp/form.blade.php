@include('page.errors.list')
<div class="form-group col-md-12">
	{!! Form::label('text','Text : ') !!}
	{!! Form::textarea('text',null, ['class' => 'form-control','rows'=>'7']) !!}
</div>
<div class="form-group col-md-12">
	{!! Form::submit($buttonSubmit,['class' => 'btn btn-info']) !!}
</div>