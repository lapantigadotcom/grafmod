@extends('app')

@section('content')
<section class="content-header">
	<h1>
		Dashboard
		<small>Control panel</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Dashboard</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">

	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">
						Komentar
					</h3>                                    
				</div>
				<div class="box-body table-responsive">
					@include('page.partials.notification')
					<table class="table" id="dataTables">
						<thead>
							<tr>
								<th>Nama</th>
								<th>Deskripsi</th>
								<th>Post</th>
							</tr>
						</thead>
						<tbody>
							@foreach($data['content'] as $row)
							<tr>
								<td>
									{{ $row->name }}<br>
									<a href="mailto:{{ $row->email }}">{{ $row->email }}</a>
									<div class="action-post-hover">
										{!! HTML::link(route('vrs-admin.comment.approve',[ $row->id,($row->enabled=='0'?'1':'0') ]),($row->enabled=='0'?'approve':'unapprove')) !!}
										<a href="javascript:void(0);" onclick="deleteModal(this)" data-href="{!! route('vrs-admin.comment.delete',[ $row->id ]) !!}" style="color:red;">trash</a>
									</div>
								</td>
								<td>{{ $row->description }}</td>
								<td>
									<a href="{!! route('home.post',[ $row->ms_post_id ]) !!}" target="_blank">Lihat Post</a>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
@include('page.scripts.delete-modal')
@endsection

@section('custom-head')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/datatables/media/css/jquery.dataTables.css') }}">
@stop
@section('custom-footer')
<script type="text/javascript" src="{{ asset('plugins/datatables/media/js/jquery.dataTables.js') }}"></script>
<script type="text/javascript">
	$('#dataTables').DataTable();
</script>
@stop