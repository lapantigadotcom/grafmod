@extends('app')

@section('content')
<section class="content-header">
	<h1>
		Dashboard
		<small>Control panel</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Dashboard</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">

	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">
						Ujian
					</h3>
				</div>
				<div class="box-body table-responsive">
					@include('page.partials.notification')
					<div class="row">
						<div class="col-md-3">
							<h5>Tambah Ujian Baru</h5>
							{!! Form::open(array('route' => 'vrs-admin.ujian.store', 'method' => 'post')) !!}
							<?php 
							$arrujian = array();
							$arrujian[0] = 'Tidak Ada';
							foreach($data['content'] as $row)
							{
								$arrujian[$row->id] = $row->title;
							}
							?>
							@include('page.ujian.form',['buttonSubmit' => 'Tambah','arrujian' => $arrujian])
							{!! Form::close()!!}
						</div>
						<div class="col-md-9">
							<div class="row">

							</div>
							<table class="table" id="dataTables">
								<thead>
									<tr>
										<th>&nbsp;</th>
										<th>Nama</th>
										<th>Deskripsi</th>
									</tr>
								</thead>
								<tbody>
									@foreach($data['content'] as $row)
									<tr>
										<td>&nbsp;</td>
										<td>
											{{ $row->title }}
											<div class="action-post-hover">
												<a href="{{route('vrs-admin.ujian.edit',[ $row->id ])}}">edit</a>
												<a href="javascript:void(0);" onclick="deleteModal(this)" data-href="{{ URL::route('vrs-admin.ujian.delete',[ $row->id ]) }}" style="color:red;">trash</a>
											</div>
										</td>
										<td>{{ $row->description }}</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@include('page.partials.media')
@include('page.scripts.delete-modal')
@endsection

@section('custom-head')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/datatables/media/css/jquery.dataTables.css') }}">
@stop
@section('custom-footer')
<script type="text/javascript" src="{{ asset('plugins/dataTables/media/js/jquery.dataTables.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/lazyload/jquery.unveil.js') }}"></script>
<script type="text/javascript">
	$('#dataTables').DataTable();
	$('.img-lazy').unveil();
</script>
@include('page.scripts.media')
@stop