@extends('app')

@section('content')
<section class="content-header">
	<h1>
		Dashboard
		<small>Control panel</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Dashboard</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">

	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">
						Mitra
						<a href="{{route('vrs-admin.user.create')}}" class="btn btn-default btn-add-new">Tambah Baru</a>
						
					</h3>                                    
				</div>
				<div class="box-body table-responsive">
					@include('page.partials.notification')
					<table class="table" id="dataTables">
						<thead>
							<tr>
								<th>&nbsp;</th>
								<th>Enagic ID</th>
								<th>Nama</th>
								<th>Panggilan</th>
								<th>No. HP</th>
								<th>Email</th>
								<th>Facebook</th>
								<th>Alamat</th>
								<th class="col-md-2">Aktif?</th>
							</tr>
						</thead>
						<tbody>
							<?php $i = 1; ?>
							@foreach($data['content'] as $row)
							<?php
								if($row->ms_privilige_id == 1)
									continue;
							?>
							<tr>
								<td>{{ $i++ }}</td>
								<td>{{ $row->detail->enagic_id!=''?$row->detail->enagic_id:'' }}</td>
								<td>
									{{ $row->name }}
									<div class="action-post-hover">
										<a href="{{route('vrs-admin.user.edit',[ $row->id ])}}">edit</a>
										<a href="javascript:void(0);" onclick="deleteModal(this)" data-href="{!! route('vrs-admin.user.delete',[ $row->id ]) !!}" style="color:red;">trash</a>
									</div>
								</td>
								<td>{{ $row->detail->panggilan!=''?$row->detail->panggilan:'' }}</td>
								<td>{{ $row->detail->nohp!=''?$row->detail->nohp:'' }}</td>
								<td>{{ $row->email!=''?$row->email:'' }}</td>
                          		<td><a href="//{{ $row->detail->facebook!=''?$row->detail->facebook:'' }}">{{ $row->detail->facebook!=''?$row->detail->facebook:'' }}</a></td>								
								<td>{{ $row->detail->address!=''?$row->detail->address:'' }}</td>								
								<td>
									<div class="Switch Round {!! $row->active=='1'?'On':'Off' !!}" data-id="{!! $row->id !!}">
										<div class="Toggle"></div>
									</div>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
@include('page.scripts.delete-modal')
@endsection

@section('custom-head')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/datatables/media/css/jquery.dataTables.css') }}">
@stop
@section('custom-footer')
<script type="text/javascript" src="{{ asset('plugins/datatables/media/js/jquery.dataTables.js') }}"></script>
<script type="text/javascript">
	$('#dataTables').DataTable();
	$('#dataTables').on('click', '.Switch', function()  {
		var id_post = $(this).data("id");
		setFeatured(id_post);
		$(this).toggleClass('On').toggleClass('Off');
	});
	function setFeatured(id)
	{
		var url = "{{ URL::to('vrs-admin/user/featured') }}/" + id +"";
		$.ajax(url)
		.done(function() {
			
		})
		.fail(function() {
			alert( "error" );
		});
	}
</script>
@stop
