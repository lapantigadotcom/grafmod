@extends('app')

@section('content')
<section class="content-header">
	<h1>
		Dashboard
		<small>Control panel</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Dashboard</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">

	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">
						SOAL
					</h3>
				</div>
				<div class="box-body table-responsive">
					@include('page.partials.notification')
					<div class="row">
						<div class="col-md-3">
							<h5>Tambah Soal Baru</h5>
							{!! Form::open(array('route' => 'vrs-admin.soal.store', 'method' => 'post')) !!}
							<?php 
							$arrsoal = array();
							$arrsoal[0] = 'Tidak Ada';
							foreach($data['content'] as $row)
							{
								$arrsoal[$row->id] = $row->pertanyaan;
							}
							?>
							@include('page.soal.form',['buttonSubmit' => 'Tambah','arrsoal' => $arrsoal, 'ujian' => $data['ujian']])
							{!! Form::close()!!}
						</div>
						<div class="col-md-9">
							<div class="row">

							</div>
							<table class="table" id="dataTables">
								<thead>
									<tr>
										<th>&nbsp;</th>
										<th>Pertanyaan</th>
										<th>Jawaban</th>
									</tr>
								</thead>
								<tbody>
									@foreach($data['content'] as $row)
									<tr>
										<td>&nbsp;</td>
										<td>
											{{ $row->pertanyaan }}
											<div class="action-post-hover">
												<a href="{{route('vrs-admin.soal.edit',[ $row->id ])}}">edit</a>
												<a href="javascript:void(0);" onclick="deleteModal(this)" data-href="{{ URL::route('vrs-admin.soal.delete',[ $row->id ]) }}" style="color:red;">trash</a>
											</div>
										</td>
										<td>
											@if($row->jawaban_benar == 'a')
												{{ $row->jawaban_a}}
											@elseif($row->jawaban_benar == 'b')
												{{ $row->jawaban_b}}
											@elseif($row->jawaban_benar == 'c')
												{{ $row->jawaban_c}}
											@elseif($row->jawaban_benar == 'd')
												{{ $row->jawaban_d}}
											@endif
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@include('page.partials.media')
@include('page.scripts.delete-modal')
@endsection

@section('custom-head')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/datatables/media/css/jquery.dataTables.css') }}">
@stop
@section('custom-footer')
<script type="text/javascript" src="{{ asset('plugins/chartjs/Chart.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/datatables/media/js/jquery.dataTables.js') }}"></script>
<script type="text/javascript">
	$('#dataTables').DataTable();
</script>
@include('page.scripts.media')
@stop