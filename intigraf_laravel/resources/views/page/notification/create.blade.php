@extends('app')

@section('content')
<script src='https://cdn.firebase.com/js/client/2.2.1/firebase.js'></script>
<section class="content-header">
	<h1>
		Dashboard
		<small>Control panel</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Dashboard</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">

	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">
						Kirim Notifikasi ke Aplikasi
					</h3>                                    
				</div>
				<div class="box-body">		
					{!! Form::open(array('url' => 'vrs-admin/notification', 'method' => 'post')) !!}
					<div class="col-md-9">
						@include('page.errors.list')
						@include('page.notification.form',['submitText' => 'Kirim'])
					</div>
					{!! Form::close() !!}
					<div class="col-md-9">
						<div class="row">

						</div>
						<table class="table" id="dataTables">
							<thead>
								<tr>
									<th>&nbsp;</th>
									<th>Deskripsi</th>
									<th>Waktu Publish</th>
								</tr>
							</thead>
							<tbody>
								@foreach($data['notif'] as $row)
								<tr>
									<td>&nbsp;</td>
									<td>{{ $row->description }}</td>
									<td>{{ $row->created_at }}</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection
@section('custom-head')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/datatables/media/css/jquery.dataTables.css') }}">
@stop
@section('custom-footer')
<script type="text/javascript" src="{{ asset('plugins/chartjs/Chart.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/datatables/media/js/jquery.dataTables.js') }}"></script>
<script type="text/javascript">
	$('#dataTables').DataTable();
</script>
@stop