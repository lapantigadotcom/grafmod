@extends('app')

@section('content')
<section class="content-header">
	<h1>
		Dashboard
		<small>Control panel</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Dashboard</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">

	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">
						Ganti Password
					</h3>                                    
				</div>
				<div class="box-body">
					@include('page.errors.list')
					@include('page.partials.notification')
					{!! Form::open(array('route' => 'vrs-admin.admin.password','method' => 'post' )) !!}
					<div class="form-group">
						{!! Form::label('oldpassword','Password Lama : ') !!}
						{!! Form::password('oldpassword',array('class' => 'form-control')) !!}
					</div>
					<div class="form-group">
						{!! Form::label('password','Password Baru : ') !!}
						{!! Form::password('password',array('class' => 'form-control')) !!}
					</div>
					<div class="form-group">
						{!! Form::label('password_confirmation','Konfirmasi Password Baru : ') !!}
						{!! Form::password('password_confirmation',array('class' => 'form-control')) !!}
					</div>
					<div class="form-group">
						{!! Form::submit('Ganti Password',['class' => 'btn btn-info']) !!}
					</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</section>
@endsection
