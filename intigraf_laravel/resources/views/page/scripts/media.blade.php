	
	<script type="text/javascript" src="{{ asset('plugins/lazyload/jquery.unveil.js') }}"></script>	
	<script src="{{ asset('plugins/bootstrapFileStyle/bootstrap-filestyle.min.js') }}"></script>
	<script src="{{ asset('plugins/lazyload/jquery.lazyload.min.js') }}"></script>
	<script src="{{ asset('plugins/loader/loadingoverlay.min.js') }}"></script>
	<script type="text/javascript">
		$('input[type=file]').filestyle();
		$(function() {
		    $("img.lazy").lazyload();
		});
	</script>
	<script type="text/javascript">
		$('#mediaUnset').hide();
		@if (isset($featuredImage))
		$('#featuredImage').empty();
		var featuredImage = "<img class='img-responsive' src='{{ asset('upload/media/') }}/{{ $featuredImage }}' />";
		$('#featuredImage').append(featuredImage);
		$('#mediaUnset').show();
		@endif
		function setFeaturedImage()
		{
			$('#mediaTab a').click(function (e) {
				e.preventDefault();
				$(this).tab('show');
			});
			mediaFillContent();
			$('#mediaModal').modal({
				backdrop:false,
				show:true
			});
		}
		function mediaFillContent()
		{
			$('#listMedia').empty();
			$.get("{{ url('vrs-admin/media/getAllData') }}",
				{

				},
				function(data,status)
				{
					if(status=='success')
					{
						console.log(data);
						var html = '';
						for(var index in data) { 
							if (data.hasOwnProperty(index)) {
								var attr = data[index];
								console.log(attr);
								html += '<div class="col-md-2 img-pop"><div class="img-media-modal-container"><img data-original="{{ asset('upload/media') }}/' + attr.file +'" class="img-media-modal lazy" onclick="mediaSelect(this)" data-media-id="' + attr.id + '" data-media-name="' + attr.file + '" /></div></div>';
							}
						}

					}
					$('#listMedia').append(html);
					$("img.lazy").lazyload({skip_invisible : false, effect : "fadeIn"});
				}
			);
		}
		function mediaFill2(x)
		{
			var y = x.getAttribute("data-offset");
			var z = parseInt(y) + 12;
			//alert(z);
			$.get("{{ url('vrs-admin/media/getAllData') }}",
				{
 					offset: y
				},
				function(data,status)
				{
					if(status=='success')
					{
						console.log(data);
						var html = '';
						for(var index in data) { 
							if (data.hasOwnProperty(index)) {
								var attr = data[index];
								console.log(attr);
								html += '<div class="col-md-2 img-pop"><div class="img-media-modal-container"><img data-original="{{ asset('upload/media') }}/' + attr.file +'" class="img-media-modal lazy" onclick="mediaSelect(this)" data-media-id="' + attr.id + '" data-media-name="' + attr.file + '" /></div></div>';
							}
						}

					}
					$('#listMedia').append(html);
					$("img.lazy").lazyload({skip_invisible : false, effect : "fadeIn"});
					// $('#btn-more').data('offset',y+12); 
					$('#btn-more').attr("data-offset",z); 
				}
			);
		}
		function mediaSelect(x)
		{
			$('#listMedia').find('.img-media-modal').removeClass('media-selected');
			$(x).addClass('media-selected');
			// var id = x.getAttribute("data-media-id");
			// $('#featuredImageInput').val(id);
		}
		function setFeatured(){
			var x = $('#listMedia').find('.media-selected');
			if($(x).length > 0)
			{

				var id = $('.media-selected:first').data("media-id");
				console.log(id);
				$('#ms_media_id').val(id);
				$('#mediaModal').modal('toggle');
				var name = $('.media-selected:first').data("media-name");
				if(id !== ''){
					$('#featuredImage').empty();
					var html = "<img class='img-responsive' src='{{ asset('upload/media/') }}/"+ name +"' />";
					$('#featuredImage').append(html);
				}
			}
		}
		function mediaUnset(){
			$('#ms_media_id').val("");
			$('#featuredImage').empty();
			$('#mediaUnset').hide();
		}
		$(document).ready(function (e) {
			$('#mediaUploadForm').on('submit',(function(e) {
				alert('uploading');
				// e.preventDefault();
				var formData = new FormData(this);
				console.log('formdata',formData);
				$.LoadingOverlay("show");
				$('#mediaBrowse').prop('disabled', true);
				alert($(this).attr('action'));
				$.ajax({
					type:'POST',
					url: $(this).attr('action'),
					data: formData,
					cache:false,
					contentType: false,
					processData: false,
					success:function(data){
						$.LoadingOverlay("hide");
						$('#mediaBrowse').prop('disabled', false);
						alert('success');
						mediaFillContent();
					},
					error: function(data){
						console.log("error");
						console.log(data);
						$.LoadingOverlay("hide");
						alert('error');
						$('#mediaBrowse').prop('disabled', false);
					}
				});
				e.preventDefault();
			}));

			$("#mediaBrowse").on("change", function() {
				$("#mediaUploadForm").submit();
			});
		});
		</script>
		