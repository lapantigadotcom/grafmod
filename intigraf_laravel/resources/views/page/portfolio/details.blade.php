@extends('app')

@section('content')
<section class="content-header">
	<h1>
		Dashboard
		<small>Control panel</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Dashboard</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">

	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">
						PORTFOLIO <em><strong> {{  $data['content']->title }} </strong></em>
					</h3>                                    
				</div>
				<div class="box-body ">
					@include('page.partials.notification')
					<div class="row">
						<div class="col-md-3">
							<h5>Tambah Detail Portfolio Baru</h5>
							@include('page.errors.list')
							{!! Form::open(['route'=>'detailportfolio.store', 'method' => 'post']) !!}
							@include('page.portfolio.form-detail',['buttonSubmit' => 'Tambah','ms_portfolio_id' => $data['content']->id ])
							{!! Form::close() !!}
						</div>
						<div class="col-md-7">
							<div class="row">
								
							</div>
							<table class="table" id="dataTables">
								<thead>
									<tr>
										<th>&nbsp;</th>
										<th>File</th>
										<th>Deskripsi</th>
									</tr>
								</thead>
								<tbody>
									@foreach($data['content']->detailPortfolio as $row)
									<tr>
										<td>&nbsp;</td>
										<td>
											<div class="row">
												<div class="col-md-5">
												@if($row->type=='1')
													<img src="{{asset('upload/media/'.$row->media->file)}}" alt="{{$row->media->alt_text}}" class="col-md-9">
													@else
													<a href="{{url($row->link)}}" target="_blank">lihat video</a>
													@endif
												</div>
												<div class="col-md-7">
													{{ $row->caption }}
													<div class="action-post-hover">
														<a href="{{route('detailportfolio.edit',[ $row->id ])}}" >edit</a>
														
														<a href="javascript:void(0);" onclick="deleteModal(this)" data-href="{!! route('detailportfolio.delete',[ $row->id ]) !!}" style="color:red;">trash</a>
													</div>
												</div>
											</div>
											
										</td>
										<td>{{ $row->description }}</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@include('page.scripts.delete-modal')
@include('page.partials.media')
@endsection

@section('custom-head')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/datatables/media/css/jquery.dataTables.css') }}">
@stop
@section('custom-footer')
<script type="text/javascript" src="{{ asset('plugins/datatables/media/js/jquery.dataTables.js') }}"></script>
<script type="text/javascript">
	$('#dataTables').DataTable();
</script>
@include('page.scripts.media')
<script type="text/javascript">
					$(document).ready(function() {
						// var reference = (function tipeportfolio(){
						// 	var type = $('#type').val();
						// 	switch(type){
						// 		case '1':
						// 			$('#mediaformcontainer').show();
						// 		break;
						// 		case '2':
						// 			$('#mediaformcontainer').hide();
						// 		break;
						// 	}
						//     return tipeportfolio;
						// }());
						// $('#type').change(function(){
						// 	reference();
						// 	console.log("ganti");
						// });
					});
					</script>
@stop