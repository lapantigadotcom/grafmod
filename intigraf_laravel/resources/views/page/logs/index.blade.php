@extends('app')

@section('content')
<section class="content-header">
	<h1>
		Dashboard
		<small>Log Systems</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Log Systems</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">

	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">
						Log Systems
					</h3>                                    
				</div>
				<div class="box-body table-responsive">
 					<table class="table" id="dataTables">
						<thead>
							<tr>
								<th>ID</th>
								<th>User</th>
								<th>Nama</th>
								<th>URL</th>
								<th>URL</th>
								<th>Tanggal</th>
							</tr>
						</thead>
						<tbody>
							@foreach($data['content'] as $row)
							<tr>
								<td>{{ $row->id }}</td>
								<td>{{ $row->ms_user_id }}</td>
								<td>{{$row->name}}</td>
								<td>{{$row->logable_type}}</td> 
								<td>{{$row->action}}</td> 
								<td>{{ date('d/m/Y | h:i:s A',strtotime($row->date)) }}</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
@include('page.scripts.delete-modal')
@endsection

@section('custom-head')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/datatables/media/css/jquery.dataTables.css') }}">
@stop
@section('custom-footer')
<script type="text/javascript" src="{{ asset('plugins/datatables/media/js/jquery.dataTables.js') }}"></script>
<script type="text/javascript">
	$('#dataTables').DataTable();
	$('#dataTables').on('click', '.Switch', function()  {
		var id_aduan = $(this).data("id");
		setFeatured(id_aduan);
		$(this).toggleClass('On').toggleClass('Off');
	});
	function setFeatured(id)
	{
		var url = "{{ URL::to('vrs-admin/aduan/featured') }}/" + id +"";
		$.ajax(url)
		.done(function() {
			
		})
		.fail(function() {
			alert( "error" );
		});
	}
</script>
@stop
