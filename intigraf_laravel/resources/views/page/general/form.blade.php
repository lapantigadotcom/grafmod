					<div class="form-group">
						{!! Form::label('site_title','Judul Website : ') !!}
						{!! Form::text('site_title',null, ['class' => 'form-control']) !!}
					</div>
					<div class="form-group">
						{!! Form::label('tagline','Tagline : ') !!}
						{!! Form::text('tagline',null, ['class' => 'form-control']) !!}
					</div>
					<div class="form-group">
						{!! Form::label('meta_description','Meta Deskripsi : ') !!}
						{!! Form::textarea('meta_description',null, ['class' => 'form-control','rows'=>'3']) !!}
					</div>
					<div class="form-group">
						{!! Form::label('meta_keywords','Meta Kata Kunci : ') !!}
						{!! Form::textarea('meta_keywords',null, ['class' => 'form-control','rows'=>'3']) !!}
					</div>
					<div class="form-group">
						{!! Form::label('about_community','About Web : ') !!}
						{!! Form::textarea('about_community',null, ['class' => 'form-control','rows'=>'3']) !!}
					</div>
					<div class="form-group">
						{!! Form::submit($buttonSubmit,['class' => 'btn btn-info']) !!}
					</div>