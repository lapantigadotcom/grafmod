@extends('app')

@section('content')
<section class="content-header">
	<h1>
		Dashboard
		<small>Control panel</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Dashboard</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">

	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">
						PENGATURAN UMUM
					</h3>                                    
				</div>
				<div class="box-body table-responsive">
					<div class="col-md-9">
					@include('page.partials.notification')
					{!! Form::model($data['content'],['action' => ['GeneralController@update',$data['content']->id], 'method' => 'PATCH' ] ) !!}
					@include('page.general.form',['buttonSubmit' => 'Perbarui'])
					{!! Form::close() !!}
					</div>
					<div class="col-md-3">
						<h4>LOGO WEBSITE / IKON </h4>
						{!! Form::open(['route' => 'general.upload', 'method' => 'POST', 'files' => true ] ) !!}
							<p>
								<img src="{{ asset('upload/media/'.$data['content']->file) }}" class="img-responsive">
							</p>
							<div class="form-group">
								{!! Form::file('file',array('class' => 'form-control')) !!}
							</div>
							<div class="form-group">
								{!! Form::submit('Perbarui',['class' => 'btn btn-info']) !!}
							</div>
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection

@section('custom-head')
@stop
@section('custom-footer')
	<script src="{{ asset('plugins/bootstrapFileStyle/bootstrap-filestyle.min.js') }}"></script>
	<script type="text/javascript">
		$('input[type=file]').filestyle({buttonText : " Ganti Gambar" });
	</script>
@stop