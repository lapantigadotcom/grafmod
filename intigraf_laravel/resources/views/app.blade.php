<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>212Mart Panel</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        @include('partials.head')
    </head>
    <body class="skin-blue">
        <div class="wrapper">
            @include('partials.nav')
            <aside class="left-side sidebar-offcanvas">
                @include('partials.side-bar')
            </aside>
            <aside class="right-side">
                @yield('content')
            </aside>

        </div>
        @include('partials.footer')
         <footer class="footer" style="background: #f4f4f4;color: #444"><center><h5><a target="_blank" href="www.lapantiga.com"><i class="icon-user"></i> Author :lapantiga.com | <i class="icon-envelope"></i> email: info@lapantiga.com</a></h5></center> </footer>
    </body>
</html>
