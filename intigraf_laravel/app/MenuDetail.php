<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class MenuDetail extends Model {

	protected $table = 'tr_menu_details';
	protected $fillable = array('ms_menu_id','parent_id','ms_menu_type_id','order','custom','title','code');
	public $timestamps = false;

	public function menu()
	{
		return $this->belongsTo('App\Menu','ms_menu_id','id');
	}
	public function menuType()
	{
		return $this->belongsTo('App\MenuType','ms_menu_type_id','id');
	}
	public function parent()
	{
		return $this->belongsTo('App\MenuDetail','parent_id','id');
	}
	public function submenu()
	{
		return $this->hasMany('App\MenuDetail','parent_id','id')->orderBy('order');
	}
	public function logs()
    {
        return $this->morphMany('App\Log', 'logable');
    }

}
