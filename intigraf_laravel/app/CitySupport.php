<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Province;
use App\General;
use App\User;

class CitySupport extends Model {

	protected $table = 'ms_support_cities';
	protected $guarded = ['id'];
    public $timestamps = false;
    public $incrementing = true;
    protected $primaryKey = 'id';
    protected $fillable = array(
        'city_id',
        'name'
    );    

}
