<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Portfolio extends Model {

	protected $table = 'ms_portfolios';
	protected $fillable = array('title','description','parent_id','enabled','featured','ms_categories_id');
	public $timestamps = true;

	public function detailPortfolio()
    {
        return $this->hasMany('App\DetailPortfolio','ms_portfolio_id','id');
    }
    public function logs()
    {
        return $this->morphMany('App\Log', 'logable');
    }
    public function subportfolio()
    {
        return $this->hasMany('App\Portfolio','parent_id','id');
    }
    public function parent()
    {
        return $this->belongsTo('App\Portfolio','parent_id','id');
    }
    public function category()
    {
        return $this->hasOne('\App\PortfolioCategory', 'id', 'ms_categories_id');
    }
}
