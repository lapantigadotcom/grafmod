<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model {

	protected $table = 'ms_notification';
	protected $fillable = array('title','description');
	public $timestamps = true;
}
