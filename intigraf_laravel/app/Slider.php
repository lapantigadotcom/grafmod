<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model {

	protected $table = 'ms_sliders';
	protected $fillable = array('title','ms_media_id','link','description', 'h1', 'h2', 'h3');
	public $timestamps = true;
	public function media()
	{
		return $this->belongsTo('\App\Media','ms_media_id','id');
	}
	public function logs()
    {
        return $this->morphMany('App\Log', 'logable');
    }
}
