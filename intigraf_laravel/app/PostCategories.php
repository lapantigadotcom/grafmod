<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class PostCategories extends Model {

	protected $table = 'tr_category_posts';
	public $timestamps = false;
	public function media()
	{
		return $this->belongsTo('\App\Media','ms_media_id','id');
	}
	public function logs()
    {
        return $this->morphMany('App\Log', 'logable');
    }
}
