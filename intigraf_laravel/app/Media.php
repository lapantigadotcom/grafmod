<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Media extends Model {

	protected $table = 'ms_medias';
	protected $fillable = array('file','mime','caption','alternate_text','description','isumum');
	public $timestamps = true;

    public function post()
    {
        return $this->belongsToMany('App\Post','tr_media_posts','ms_media_id','ms_post_id');
    }
    public function portfolio()
    {
    	return $this->hasMany('App\Portfolio','ms_media_id','id');
    }
    public function logs()
    {
        return $this->morphMany('App\Log', 'logable');
    }
}
