<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Gender extends Model {

	protected $table = 'ms_gender';
	public $timestamps = false;
}
