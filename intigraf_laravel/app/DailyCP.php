<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DailyCP extends Model
{
    //
    protected $table = 'ms_dailycp';
    protected $fillable = ['text'];
}
