<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Download extends Model {

	protected $table = 'ms_download';
	public $timestamps = false;
}