<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model {

    protected $table = 'ms_posts';
    protected $fillable = array('title','title_eng','description', 'eng_description','meta_description','ms_media_id','ms_user_id','ms_status_posts_id','ms_portfolio_id','show_comment','featured');
    public $timestamps = true;

    public function user()
    {
        return $this->belongsTo('App\User','ms_user_id','id');
    }
    public function statusPost()
    {
        return $this->belongsTo('App\StatusPost','ms_status_posts_id','id');
    }
    public function tag()
    {
        return $this->belongsToMany('App\Tag','tr_post_tags','ms_post_id','ms_tag_id');
    }
    public function category()
    {
        return $this->belongsToMany('App\Category','tr_category_posts','ms_post_id','ms_category_id');
    }
    public function mediaPost()
    {
        return $this->belongsTo('App\Media','ms_media_id','id');
    }
    public function portfolio()
    {
        return $this->belongsTo('App\Portfolio','ms_portfolio_id','id');
    }
    public function media()
    {
        return $this->belongsToMany('App\Media','tr_media_posts','ms_post_id','ms_media_id');
    }
    public function comment()
    {
        return $this->hasMany('App\Comment','ms_post_id','id');
    }
    public function logs()
    {
        return $this->morphMany('App\Log', 'logable');
    }
    public function photo()
    {
        return $this->hasOne('\App\Media', 'id', 'ms_media_id');
    }
}
