<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use Input;

class TestimoniRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		$rule = [
			'description' => 'required',
			'file' => 'required|image'
		];
		// if(Input::get('password') != Input::get('password_conf'))
		// {
		// 	$rule['password'] = 'required';
		// 	$rule['password_conf'] = 'required';
		// }
		return $rule;
	}

	/**
	 * Get the error messages for the defined validation rules.
	 *
	 * @return array
	 */
	public function messages()
	{
	    return [
	        'description.required' => 'Deskripsi harus diisi',
	        'file.required'  => 'Harus mengunggah foto'
	    ];
	}

}
