<?php namespace App\Http\Controllers;
/*
lapantiga.com | Web & Mobile App Developer. Jl. Gubeng Kertajaya 9C no.27 A Surabaya - Indonesia, +62.856.3437.495 */

use App\TypeClub;
use App\Http\Controllers\BasicController;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\TypeClubRequest;
use Illuminate\Http\Request;
use Session;
use Image;
use File;

class TypeClubController extends BasicController {

	public function index(){
		$data['content'] = TypeClub::with('club')->get();
		return view('page.typeclub.index',compact('data'));
	}

	public function create(){
		
	}
	public function store(TypeClubRequest $request){
		$data = TypeClub::create($request->all());
		Session::flash('success','Data berhasil ditambahkan');
		return redirect()->route('vrs-admin.typeclub.index');
	}
	public function edit($id)
	{
		$data['content']=TypeClub::find($id);

		return view('page.typeclub.edit',compact('data'));
	}

	public function update(TypeClubRequest $request,$id)
	{

		$data = TypeClub::find($id);
		$data->update($request->all());


		Session::flash('success','Data berhasil diperbarui');
		return redirect()->route('vrs-admin.typeclub.index');
	}

	public function destroy($id)
	{
		$data = TypeClub::with('club')->find($id);
		if($data->club()->count() > 0){
			foreach ($data->club as $row) {
				if(File::exists('upload/member/'.$row->photo))
				{
					File::delete('upload/member/'.$row->photo);
				}
				$row->delete();
			}
		}
		$data->delete();
		Session::flash('success','Data berhasil dihapus');
		return redirect()->route('vrs-admin.typeclub.index');
	}
}
