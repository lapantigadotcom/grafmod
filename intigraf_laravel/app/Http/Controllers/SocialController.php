<?php namespace App\Http\Controllers;
/*
lapantiga.com | Web & Mobile App Developer. Jl. Gubeng Kertajaya 9C no.27 A Surabaya - Indonesia, +62.856.3437.495 */

use App\Http\Controllers\BasicController;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\SocialRequest;
use App\Social;
use Illuminate\Http\Request;
use Session;

class SocialController extends BasicController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['content'] = Social::all();
		return view('page.social.index',compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(SocialRequest $request)
	{
		$data = Social::create($request->all());
		$log = array();
		$log['action'] = 'insert';
		$log['name'] = $request->input('name');
		$data->logs()->save($this->log($log));
		Session::flash('success','Data berhasil ditambahkan');
		return redirect()->route('vrs-admin.social.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data['content'] = Social::find($id);
		return view('page.social.edit',compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(SocialRequest $request, $id)
	{
		$data = Social::find($id);
		$data->update($request->all());
		$log = array();
		$log['action'] = 'update';
		$log['name'] = $request->input('name');
		$data->logs()->save($this->log($log));

		Session::flash('success','Data berhasil diperbarui');
		return redirect()->route('vrs-admin.social.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$data = Social::find($id);
		$log = array();
		$log['action'] = 'delete';
		$log['name'] = $data->name;
		$data->logs()->save($this->log($log));
		$data->delete();
		Session::flash('success','Data berhasil dihapus');
		return redirect()->route('vrs-admin.social.index');
	}

}
