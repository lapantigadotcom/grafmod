<?php namespace App\Http\Controllers;
/*
lapantiga.com | Web & Mobile App Developer. Jl. Gubeng Kertajaya 9C no.27 A Surabaya - Indonesia, +62.856.3437.495 */

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\RssRequest;
use App\MenuDetail;
use App\Rss;
use Illuminate\Http\Request;
use Session;

class RssController extends BasicController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['content'] = Rss::all();
		return view('page.rss.index')->with('data',$data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(RssRequest $request)
	{
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(RssRequest $request)
	{
		$data = Rss::create($request->all());

		$log = array();
		$log['action'] = 'insert';
		$log['name'] = $data->title;
		$data->logs()->save($this->log($log));

		Session::flash('success','Data berhasil ditambahkan');
		return redirect()->route('vrs-admin.rss.index');
		
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data['content']=Rss::find($id);
		return view('page.rss.edit',compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request,$id)
	{
		$data = Rss::find($id);
		$data->update($request->all());

		$log = array();
		$log['action'] = 'update';
		$log['name'] = $data->title;
		$data->logs()->save($this->log($log));

		Session::flash('success','Data berhasil diperbarui');
		return redirect()->route('vrs-admin.rss.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$data = Rss::find($id);

		$log = array();
		$log['action'] = 'delete';
		$log['name'] = $data->title;
		$data->logs()->save($this->log($log));
		$menu = MenuDetail::where('ms_menu_type_id',4)->where('custom',$id)->get();
		foreach ($menu as $row) {
			$row->delete();
		}

		$data->delete();
		Session::flash('success','Data berhasil dihapus');
		return redirect()->route('vrs-admin.rss.index');
	}
	public function getAllAjax()
	{
		$data = Rss::all();

		return  response()->json($data);
	}

}
