<?php namespace App\Http\Controllers;
/*
lapantiga.com | Web & Mobile App Developer. Jl. Gubeng Kertajaya 9C no.27 A Surabaya - Indonesia, +62.856.3437.495 */

use App\Http\Controllers\BasicController;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\MenuRequest;
use App\Menu;
use App\MenuType;
use Illuminate\Http\Request;
use Session;

class MenuController extends BasicController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['content'] = Menu::with('menuDetail')->get();
		return view('page.menu.index',compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('page.menu.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(MenuRequest $request)
	{
		$data = Menu::create($request->all());

		$log = array();
		$log['action'] = 'insert';
		$log['name'] = $request->input('title');
		$data->logs()->save($this->log($log));

		Session::flash('success','Data berhasil ditambahkan');

		return redirect()->route('vrs-admin.menu.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$data['content'] = Menu::with('menuDetail')->find($id);
		$data['menu_type'] = MenuType::all();
		return view('page.menu.details',compact('data'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data['content'] = Menu::find($id);
		return view('page.menu.edit',compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(MenuRequest $request, $id)
	{
		$data = Menu::find($id);
		$data->update($request->all());
		$log = array();
		$log['action'] = 'update';
		$log['name'] = $request->input('title');
		$data->logs()->save($this->log($log));

		Session::flash('success','Data berhasil diperbarui');
		return redirect()->route('vrs-admin.menu.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$data = Menu::with('menuDetail')->find($id);
		foreach ($data->menuDetail as $row) {
			$row->delete();
		}
		$log = array();
		$log['action'] = 'delete';
		$log['name'] = $data->title;
		$data->logs()->save($this->log($log));

		$data->delete();
		Session::flash('success','Data berhasil dihapus');
		return redirect()->route('vrs-admin.menu.index');
	}
}
