<?php namespace App\Http\Controllers;
/*
lapantiga.com | Web & Mobile App Developer. Jl. Gubeng Kertajaya 9C no.27 A Surabaya - Indonesia, +62.856.3437.495 */

use App\DetailPortfolio;
use App\Http\Controllers\Controller;
// use App\Http\Requests;
use App\Http\Requests\DetailPortfolioRequest;
use Illuminate\Http\Request;
use Session;
class DetailPortfolioController extends BasicController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$data = DetailPortfolio::create([
			'type' => $request->type,
			'ms_media_id' => $request->ms_media_id,
			'caption' => empty($request->caption) ? '' : $request->caption,
			'ms_portfolio_id' => empty($request->ms_portfolio_id) ? 0 : $request->ms_portfolio_id,
			'description' => empty($request->description) ? '' : $request->description,
			'link' => empty($request->link) ? '' : $request->link,
		]);

		$log = array();
		$log['action'] = 'delete';
		$log['name'] = $data->caption;
		$data->logs()->save($this->log($log));

		Session::flash('success','Data berhasil ditambah');
		return redirect()->route('portfolio.show',[$request->ms_portfolio_id]);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data['content'] = DetailPortfolio::with('media')->find($id);
		return view('page.portfolio.detail-edit',compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request,$id)
	{
		$data = DetailPortfolio::find($id);
		$data->update($request->all());
		Session::flash('success','Data berhasil diperbarui');
		return redirect()->route('portfolio.show',$data->ms_portfolio_id);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$data = DetailPortfolio::find($id);
		$id = $data->ms_portfolio_id;
		
		$log = array();
		$log['action'] = 'delete';
		$log['name'] = $data->caption;
		$data->logs()->save($this->log($log));

		$data->delete();
		Session::flash('success','Data berhasil dihapus');
		return redirect()->route('portfolio.show',$id);
	}

}
