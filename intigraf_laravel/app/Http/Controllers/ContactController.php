<?php namespace App\Http\Controllers;
/*
lapantiga.com | Web & Mobile App Developer. Jl. Gubeng Kertajaya 9C no.27 A Surabaya - Indonesia, +62.856.3437.495 */

use App\Contact;
use App\Http\Controllers\BasicController;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\ContactRequest;
use Illuminate\Http\Request;
use Session;

class ContactController extends BasicController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['content'] = Contact::all()->first();
		return view('page.contact.index',compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('page.contact.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$data = Contact::create($request->all());

		$log = array();
		$log['action'] = 'insert';
		$log['name'] = $request->input('name');
		$data->logs()->save($this->log($log));

		Session::flash('success','Data berhasil ditambahkan');
		return redirect()->route('contact.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$id = 2;
		$data['content'] = Contact::find($id);
		return view('page.contact.edit',compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$data = Contact::find($id);
		$data->update([
			'facebook' => empty($request->facebook) ? '' : $request->facebook,
			'twitter' => empty($request->twitter) ? '' : $request->twitter,
			'instagram' => empty($request->instagram) ? '' : $request->instagram,
			'address' => empty($request->address) ? '' : $request->address,
		]);
		$log = array();
		$log['action'] = 'update';
		$log['name'] = $request->input('name');
		$data->logs()->save($this->log($log));

		Session::flash('success','Data berhasil diperbarui');
		return redirect()->route('contact.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$data = Contact::find($id);
		$log = array();
		$log['action'] = 'delete';
		$log['name'] = $data->name;
		$data->logs()->save($this->log($log));

		$data->delete();
		Session::flash('success','Data berhasil dihapus');
		return redirect()->route('contact.index');
	}
	public function enabled($id=null)
	{
		if(empty($id))
			return;
		$data = Contact::where('featured','1')->first();
		if(!empty($data))
		{
			$data->featured = 0;
			$data->save();
		}
		$data = Contact::find($id);
		$data->featured = '1';
		$data->save();
		return;
	}

}
