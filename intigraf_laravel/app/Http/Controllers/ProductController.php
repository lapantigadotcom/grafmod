<?php namespace App\Http\Controllers;
/*
lapantiga.com | Web & Mobile App Developer. Jl. Gubeng Kertajaya 9C no.27 A Surabaya - Indonesia, +62.856.3437.495 
*/

use App\Category;
use App\Http\Controllers\BasicController;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\ProductRequest;
use App\MenuDetail;
use App\Portfolio;
use App\Post;
use App\Tag;
use App\Products;
use App\ProductCategories;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class ProductController extends BasicController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['content'] = Products::all();
		return view('page.product.index')->with('data',$data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */

	public function create()
	{
		$data['category'] = ProductCategories::all();
		return view('page.product.create')->with('data',$data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(ProductRequest $request)
	{
		$data=Products::create($request->all());

		Session::flash('success','Data berhasil ditambahkan');
		return redirect()->route('vrs-admin.product.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		echo "string";
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data['content'] = Products::all()->find($id);
		$data['category'] = ProductCategories::all();
		return view('page.product.edit',compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(ProductRequest $request,$id)
	{
		$data=Products::find($id);
		$data->update($request->all());

		Session::flash('success','Data berhasil diperbarui');
		return redirect()->route('vrs-admin.product.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$data=Products::find($id);
		$data->delete();		

		Session::flash('success','Data berhasil dihapus');
		return redirect()->route('vrs-admin.product.index');
	}

	public function getAllAjax()
	{
		$data = Products::all();
		return response()->json($data);
	}
}
