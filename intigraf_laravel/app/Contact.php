<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model {

	protected $table = 'ms_contacts';
	protected $fillable = array('ms_user_id','name','email','telephone','mobile','fax','website','address','city','province','country','description','file','featured','active','facebook','twitter','instagram');
	public $timestamps = true;

	public function user()
	{
		return $this->belongsTo('App\User','ms_user_id','id');
	}
	public function logs()
    {
        return $this->morphMany('App\Log', 'logable');
    }
}
