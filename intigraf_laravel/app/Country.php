<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Province;
use App\General;
use App\User;

class Country extends Model {

	protected $table = 'ms_country';
	protected $guarded = ['id'];
    public $timestamps = false;
    public $incrementing = true;
    protected $primaryKey = 'id';
    protected $fillable = array(
        'country_id',
        'name'
    );
}
