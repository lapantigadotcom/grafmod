<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class StatusPost extends Model {

	protected $table = 'ms_status_posts';
	protected $fillable = array('title');
	public $timestamps = false;

	public function post()
    {
        return $this->hasMany('App\Post','ms_status_posts_id','id');
    }
}
