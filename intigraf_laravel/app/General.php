<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class General extends Model {

	protected $table = 'ms_general';
	protected $fillable = array('site_title','tagline','language','file','meta_description','meta_keywords','active','about_community','file_credit');
	public $timestamps = true;
	public function logs()
    {
        return $this->morphMany('App\Log', 'logable');
    }
}
