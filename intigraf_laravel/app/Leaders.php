<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Leaders extends Model {

    protected $table = 'ms_leaders';
    protected $fillable = array('name', 'rank', 'jabatan', 'ms_media_id', 'facebook', 'twitter', 'skype', 'isi');
    public $timestamps = false;

    public function achievements()
    {
        return $this->belongsToMany('\App\Achievement', 'tr_achievment', 'ms_leaders_id', 'ms_achievement_id');
    }

    public function media()
    {
        return $this->hasOne('\App\Media', 'id', 'ms_media_id');
    }
}
