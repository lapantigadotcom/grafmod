<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class MenuType extends Model {

	protected $table = 'ms_menu_types';
	protected $fillable = array('code','title');
	public $timestamps = false;

	public function menuDetail()
	{
		return $this->hasMany('App\MenuDetail','ms_menu_type_id','id');
	}
}
